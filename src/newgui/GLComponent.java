package newgui;

import gui.GUIElement;
import org.newdawn.slick.UnicodeFont;
import sun.security.action.GetPropertyAction;

import javax.accessibility.AccessibleContext;
import java.awt.*;
import java.awt.dnd.DropTarget;
import java.awt.event.*;
import java.awt.peer.ComponentPeer;
import java.beans.PropertyChangeSupport;
import java.util.Locale;
import java.util.Set;
import java.util.Vector;

public class GLComponent {
    public static final float TOP_ALIGNMENT = 0;
    public static final float CENTER_ALIGNMENT = 0.5f;
    public static final float BOTTOM_ALIGNMENT = 1;
    public static final float RIGHT_ALIGNMENT = 1;
    public static final float LEFT_ALIGNMENT = 0;

    static final Object treeLock = new String("AWT_TREE_LOCK");
    private static final Dimension DEFAULT_MAX_SIZE
            = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);

    int x;
    int y;
    int width;
    int height;

    GLColor foreground;

    GLColor background;

    UnicodeFont font;

    UnicodeFont peerFont;

    Cursor cursor;

    Locale locale = Locale.getDefault();

    boolean ignoreRepaint;

    boolean visible = true;

    boolean enabled = true;

    boolean valid;

    DropTarget dropTarget;

    Vector popups;

    String name;

    boolean nameExplicitlySet;

    boolean focusable = true;

    int isFocusTraversableOverridden;

    Set[] focusTraversalKeys;

    boolean focusTraversalKeysEnabled = true;

    Dimension minSize;

    boolean minSizeSet;

    Dimension maxSize;

    boolean maxSizeSet;

    Dimension prefSize;

    boolean prefSizeSet;

    PropertyChangeSupport changeSupport;

    boolean isPacked;

    int componentSerializedDataVersion = 4;

    AccessibleContext accessibleContext;

    transient ComponentListener componentListener;

    transient FocusListener focusListener;

    transient KeyListener keyListener;

    transient MouseListener mouseListener;

    transient MouseMotionListener mouseMotionListener;

    transient MouseWheelListener mouseWheelListener;

    transient InputMethodListener inputMethodListener;

    transient HierarchyListener hierarchyListener;

    transient HierarchyBoundsListener hierarchyBoundsListener;

    transient Container parent;

    transient ComponentPeer peer;

    transient ComponentOrientation componentOrientation = ComponentOrientation.UNKNOWN;

    transient GraphicsConfiguration graphicsConfig;

    int numHierarchyListeners;
    int numHierarchyBoundsListeners;

    private transient FocusEvent pendingFocusRequest = null;

    private static transient boolean incrementalDraw;
    private static transient Long redrawRate;

    static {
        incrementalDraw = Boolean.getBoolean("awt.image.incrementalDraw");
        redrawRate = Long.getLong("awt.image.redrawrate");
    }


}
