package gui;

import jpen.PButtonEvent;
import jpen.PLevelEvent;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import util.GLUtils;

import java.awt.*;

import static org.lwjgl.opengl.GL11.*;

public class PropertyInputFloat extends PropertyInput {

    UnicodeFont font;

    GuiProperty<Double> parentProperty;

    Mode mode = Mode.IDLE;

    public PropertyInputFloat(GuiProperty<Double> property) {
        super();


        this.parentProperty = property;
        font = new UnicodeFont(new Font(Font.SANS_SERIF, 0, 15));
        font.addAsciiGlyphs();
        font.getEffects().add(new ColorEffect(Color.BLACK));
        try {
            font.loadGlyphs();
        } catch (SlickException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void render(float x, float y, float w, float h) {


        glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);


        glColor4f(1, 1, 1, 0.5f);
        //GLUtils.fillRectangleC(x, y, w, h, 0);

        font.drawString(x + 5, y, parentProperty.getName());
        font.drawString(x + w - 40, y + 15, String.format("%1.1f", parentProperty.getValue()));
        glBindTexture(GL_TEXTURE_2D, 0);
        glEnable(GL_BLEND);

        glColor3f(.5f, .5f, .5f);
        /**
         * slider
         */
        GLUtils.drawRoundedRectangle(10, y + 25, w - 60, 4, 3);


        double f = (parentProperty.value - parentProperty.min) / (parentProperty.max - parentProperty.min);


        if (f > 1)
            f = 1;

        float offsetX = (float) f * (w - 60);


        //glColor3f(0, 0, 0);
        GLUtils.drawRoundedRectangle(6 + offsetX, y + 22, 10, 10, 5);
    }


    private boolean withinSliderBounds(float x, float y) {
        return x >= 6 && x < 156 &&
                y >= 22 && y < 32;
    }

    @Override
    public int getHeight() {
        return 40;
    }

    @Override
    public void onPress(PButtonEvent event, float x, float y, float pressure) {

        if (withinSliderBounds(x, y)) {
            mode = Mode.DRAGGING_SLIDER;
            setSliderX(x);
        }

    }

    private void setSliderX(float x) {
        x -= 10;
        x /= 140f;

        Double value = new Double(x * parentProperty.max - parentProperty.min) + parentProperty.min;
        if (value > parentProperty.max)
            value = parentProperty.max;
        if (value < parentProperty.min)
            value = parentProperty.min;
        parentProperty.setValue(value);
    }

    private float getSliderX() {
        double f = (parentProperty.value - parentProperty.min) / (parentProperty.max - parentProperty.min);


        f = Math.sqrt(f);

        return (float) f * (140);
    }


    @Override
    public void onDrag(PLevelEvent event, float x, float y, float lastX, float lastY, float pressure, float lastPressure) {
        /*if(mode = Mode.IDLE){
            double f = (parentProperty.value - parentProperty.min) / (parentProperty.max - parentProperty.min);
        }*/
        if (mode == Mode.DRAGGING_SLIDER) {
            this.setSliderX(x);
        }
    }

    @Override
    public void onRelease(PButtonEvent event, float x, float y, float pressure) {
        mode = Mode.IDLE;
    }

    boolean extended = false;

    enum Mode {
        IDLE,
        DRAGGING_SLIDER;
    }


}
