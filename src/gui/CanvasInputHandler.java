package gui;

import jpen.*;
import jpen.event.PenAdapter;
import jpen.event.PenListener;
import jpen.owner.PenOwner;
import kernel.Program;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class CanvasInputHandler implements PenListener, MouseWheelListener {


    Canvas getCanvas() {
        return Window.getCanvas();
    }


    /**
     * all variables about the current state of any mouse or stylus input
     * all fields starting with "last" contain the values of the previous even(this is to get all values for both the starting and end point of a e.g. a Mouse Drag event)
     */
    private float x, y, pressure;
    private float lastX, lastY, lastPressure;
    private PLevelEvent lastEvent;
    private GUIElement currentElement;

    /**
     * keeps track of which mouse buttons are currently pressed
     */
    boolean buttonDown[] = new boolean[7];

    public CanvasInputHandler() { }



    @Override
    public void penKindEvent(PKindEvent pKindEvent) {
    }

    @Override
    public void penLevelEvent(PLevelEvent event) {
        for (PLevel p : event.levels) {
            switch (p.getType()) {
                case X:
                    x = p.value;
                    break;
                case Y:
                    y = p.value;
                    break;
                case PRESSURE:
                    pressure = p.value;
                default:
                    break;
            }
        }

        for (int i = 0; i < buttonDown.length; i++) {
            if (buttonDown[i]) {
                if (event.getDevice().getKindTypeNumber() == 0) {
                    pressure = 1f;
                }
                mouseDragged(event);
                break;
            }
        }

        lastX = x;
        lastY = y;
        lastPressure = pressure;
        lastEvent = event;
    }

    @Override
    public void penButtonEvent(PButtonEvent event) {

        event.pen.setFrequencyLater(120);

        buttonDown[event.button.typeNumber] = event.button.value;

        switch(event.button.getType()){
            case ON_PRESSURE: return;
        }

        if (event.button.value) {
            mouseButtonPressed(event, x, y);
        } else {
            mouseButtonReleased(event, x, y);
        }
    }

    private void mouseButtonReleased(PButtonEvent event, float x, float y) {
        if(currentElement != null){
            currentElement.onRelease(event,x,y,pressure);
            currentElement = null;
        }
    }

    private void mouseButtonPressed(PButtonEvent event, float x, float y) {

        Vector2f v = new Vector2f(x, y);
        for (GUIElement e : Window.getCanvas().gui) {
            if (e.withinBounds(v)) {
                currentElement = e;

            }
        }
        if(currentElement!=null)
            currentElement.onPress(event,x,y,pressure);
    }

    @Override
    public void penScrollEvent(PScrollEvent pScrollEvent) {

    }

    @Override
    public void penTock(long l) {
    }

    private void mouseDragged(PLevelEvent event) {
        if (currentElement == null) {
            if (event.pen.getButtonValue(PButton.Type.CENTER)) {
                Program.getProject().setRotZ(Program.getProject().getRotZ() + (x - lastX) / 200f / Program.getProject().getZoom());
                Program.getProject().setRotX(Program.getProject().getRotX() - (y - lastY) / 200f / Program.getProject().getZoom());
            } else if (event.pen.getButtonValue(PButton.Type.LEFT)) {
                Program.getProject().addInputEvent(
                        new InputDragEvent(new CursorInputData(x, y, pressure, event), new CursorInputData(lastX, lastY, lastPressure, lastEvent))
                );
            }
        } else {
            currentElement.onDrag(event, x, y, lastX, lastY, pressure, lastPressure);
        }
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() > 0) {
            Program.getProject().setZoom(Program.getProject().getZoom() / 1.2f);
        } else if (e.getWheelRotation() < 0) {
            Program.getProject().setZoom(Program.getProject().getZoom() * 1.2f);
        }
    }
}



