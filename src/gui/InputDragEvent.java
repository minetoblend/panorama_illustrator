package gui;

import org.lwjgl.util.vector.Vector2f;

import java.util.Arrays;


public class InputDragEvent {

    public CursorInputData p1, p2;

    public InputDragEvent(CursorInputData p1, CursorInputData p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Vector2f[] pointList(int points){
        Vector2f[] res = new Vector2f[points];

        return res;
    }
}
