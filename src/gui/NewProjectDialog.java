package gui;

import javax.swing.*;

public class NewProjectDialog extends JFrame {

    private static NewProjectDialog instance;

    public static NewProjectDialog getInstance(){
        if(instance == null)
            instance = new NewProjectDialog();
        return instance;
    }

    private NewProjectDialog(){
        setSize(600,500);

    }

    public static void showDialog() {
        NewProjectDialog dialog = getInstance();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);

    }
}
