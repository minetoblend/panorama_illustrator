package gui;

import jpen.PLevelEvent;
import kernel.Program;
import kernel.Project;
import org.lwjgl.util.vector.Vector2f;

public class CursorInputData {

    final public float x, y;
    final public float pressure;
    final public PLevelEvent event;


    public CursorInputData(float x, float y, float pressure, PLevelEvent event) {
        this.x = x;
        this.y = y;
        this.pressure = pressure;
        this.event = event;
    }

    public Vector2f getCoordinates(){
        if(Program.getProject().getDisplayMode() == Project.DisplayMode.PROJECTED){
            return null;
        }else{
            return null;
        }
    }
}