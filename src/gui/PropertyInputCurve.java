package gui;

import jpen.PButtonEvent;
import jpen.PLevelEvent;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import util.GLUtils;

import java.awt.*;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_BLEND;

public class PropertyInputCurve extends PropertyInput {
    UnicodeFont font;

    private final GuiProperty parentProperty;
    Vector2f grabbedPoint = null;

    Mode mode = Mode.IDLE;


    PropertyInputCurve(GuiProperty parent) {
        this.parentProperty = parent;

        font = new UnicodeFont(new Font(Font.SANS_SERIF, 0, 15));
        font.addAsciiGlyphs();
        font.getEffects().add(new ColorEffect(Color.BLACK));
        try {
            font.loadGlyphs();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getHeight() {
        return parentProperty.minimized ? 20 : 200;
    }

    float activationState = 0;

    @Override
    public void render(float x, float y, float w, float h) {

        activationState = parentProperty.activated ? 1 : 0;

        font.drawString(x + 40, y, parentProperty.getName());
        glBindTexture(GL_TEXTURE_2D, 0);
        glEnable(GL_BLEND);

        float alpha = activationState / 2 + 0.5f;


        glColor4f(.5f, .5f, .5f, alpha);

        GLUtils.drawRoundedRectangle(x + 5, y + 2, 30, 16, 8);

        glColor4f(1, 1, 1, alpha);

        GLUtils.drawRoundedRectangle(x + 7 + activationState * 14, y + 4, 12, 12, 6);


        if (!parentProperty.minimized) {


            glColor4f(.5f, .5f, .5f, 0.5f);
            GLUtils.drawRectangleC(x + 20, y + 30, 150, 150, 0);

            glColor4f(.5f, .5f, .5f, alpha);

            for (Vector2f point : parentProperty.curve.getPoints()) {
                GLUtils.drawRectangle(x + 20 + point.x * 150, y + 30 + (1 - point.y) * 150, 3, 3);
                //System.out.println(point);
            }

            /**for (BezQuad point : parentProperty.curve.curves) {
             GLUtils.drawRectangle(x + 20 + point.getControl().x * 150, y + 30 + (1 - point.getControl().y) * 150, 3, 3);
             GLUtils.drawRectangle(x + 20 + point.getControl2().x * 150, y + 30 + (1 - point.getControl2().y) * 150, 3, 3);
             System.out.println(point);
             }*/




            glBegin(GL_LINE_STRIP);
            int points = 32;
            for (int i = 0; i <= points; i++) {
                float pointX = i / (float) points;
                float pointY = parentProperty.curve.evaluate(pointX);

                glVertex2f(x + 20 + pointX * 150, y + 30 + (1 - pointY) * 150);
            }
            glEnd();
        }
    }

    @Override
    public void onPress(PButtonEvent event, float x, float y, float pressure) {
        if (withinCheckboxBounds(x, y)) {
            parentProperty.setActivated(!parentProperty.activated);
        } else if (withinCurveBounds(x, y)) {
            x = (x - 20) / 150;
            y = 1 - (y - 30) / 150;

            Vector2f v = new Vector2f(x, y);
            for (Vector2f point : parentProperty.curve.getPoints()) {
                float distance = Vector2f.sub(point, v, null).length();
                if (distance < 0.1) {
                    grabbedPoint = point;
                    mode = Mode.MOVING_POINT;
                    return;
                }
            }

            grabbedPoint = new Vector2f(x, y);
            mode = Mode.MOVING_POINT;
            parentProperty.curve.addPoint(grabbedPoint);
        }
    }

    @Override
    public void onDrag(PLevelEvent event, float x, float y, float lastX, float lastY, float pressure, float lastPressure) {
        if (mode == Mode.MOVING_POINT) {
            float dX = (x - lastX) / 150f;
            float dY = -(y - lastY) / 150f;
            parentProperty.curve.movePoint(grabbedPoint, grabbedPoint.x + dX, grabbedPoint.y + dY);
            parentProperty.updated();
        }
    }

    @Override
    public void onRelease(PButtonEvent event, float x, float y, float pressure) {
        mode = Mode.IDLE;
    }

    private boolean withinCurveBounds(float x, float y) {
        return
                x >= 20 && x < 170 &&
                        y > 30 && y < 180;
    }

    private boolean withinCheckboxBounds(float x, float y) {
        return
                x >= 5 && x < 35 &&
                        y >= 2 && y < 18;

    }


    private enum Mode {
        IDLE,
        MOVING_POINT
    }
}
