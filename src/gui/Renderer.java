package gui;

import org.lwjgl.opengl.Display;

public class Renderer {
    public static void run(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                init();
                while(true){
                    Display.sync(60);
                    if(shouldRender())
                        render();
                }
            }
        }).start();
    }

    private static void render() {
        Window.render();
    }

    private static boolean shouldRender() {
        return true;
    }

    private static void init() {

    }
}
