package gui;

import jpen.PButtonEvent;
import jpen.PLevelEvent;
import kernel.ColorManager;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import resources.images.ImageLocator;
import util.GLUtils;
import util.ShaderUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.EXTFramebufferObject.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_BGRA;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL13.*;

public class ColorPicker extends GUIElement {

    /**
     * The current position of the color picker GUI element.
     * This can be in either corner of the window. -> e.g. Top Right corner of the window
     */
    Position pos = Position.BOTTOM_LEFT;
    /**
     * Wether or not the user is currently dragging the component, or changing the hue or value/saturation.
     */
    Mode mode = null;

    /**
     * the current angle the component is at.
     * the component always gets rotated so the center of the quarter circle points towards the edge of the screen
     */
    float rotation = getRotation(this.pos);
    /**
     * this variable is mainly there to keep track if the component should rotate to the left or right when being performing the rotation animation
     */
    float lastRot = getRotation(this.pos);

    /**
     * the absolute x and y coordinates of the component in pixels
     */
    float x = getAbsolutePosition(pos).x, y = getAbsolutePosition(pos).y;

    /**
     * the x and y coordinates of the component in pixels as it was on the last frame.
     * This variable exists to allow smooth animations -> x and y are blended together with lastX and lastY to create a smooth movement when dragging them
     */
    float lastX = x, lastY = y;
    /**
     * the width/height of the component in puixels
     */
    int lastSize;
    /**
     * wether or not the component needs to be redrawn
     */
    boolean isDirty = true;

    /**
     * pointer to the shader program that renders the back layer of the component
     */
    int shader;
    /**
     * pointers to uniform values to pass values to the shader
     */
    int uniformHue, uniformTexLoc, uniformTexLoc2;

    /**
     * pointers to all sorts of textures required for drawing the component
     */
    int texture, textureDragIcon, textureOutline, textureHueAlpha;

    /**
     * pointer to the framebuffer to draw the component on and the texture that contains all pixel data about the component
     * this is so the pixel data doesn't need to be re-computed on every program render
     * TODO: use FBO class isntead of manually creating it
     */
    int fbo, fboTexture;

    /**
     * hue saturation and value/brightness of the color -> see HSL color mdoel
     */
    float hue, sat = 0.5f, val = 0;

    /**
     * last angle of the mouse cursor relative of the component's coordinate system
     */
    float lastAngle;

    ColorPicker() {
        try {
            BufferedImage bg = ImageIO.read(ImageLocator.class.getResourceAsStream("colorpicker.png"));
            texture = GLUtils.getTextureFromBufferedImage(bg);

            textureDragIcon = GLUtils.getTextureFromBufferedImage(ImageIO.read(ImageLocator.class.getResourceAsStream("colorpicker_drag.png")));
            textureOutline = GLUtils.getTextureFromBufferedImage(ImageIO.read(ImageLocator.class.getResourceAsStream("colorpicker_outline.png")));
            textureHueAlpha = GLUtils.getTextureFromBufferedImage(ImageIO.read(ImageLocator.class.getResourceAsStream("colorpicker_hue_alpha.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void render() {

        int canvasSize = Math.max(Window.getCanvas().getWidth(), Window.getCanvas().getHeight());

        int size = 150;
        if (canvasSize >= 1000) {
            if (canvasSize >= 1920) {
                size = 300;
            } else {
                size = 200;
            }
        }

        if (size != lastSize) {
            /**
             * fbo needs to be resized if the size of the component changes due to e.g. an application window resize
             */
            generateFBO(size * 2);
            lastSize = size;
            isDirty = true;
        }
        /**
         * lazy loading of the shader
         */
        if (shader == 0) {
            glUseProgram(0);
            shader = ShaderUtils.getShader("colorpicker");
            uniformHue = glGetUniformLocation(shader, "hueOffset");
            uniformTexLoc = glGetUniformLocation(shader, "sampler01");
            uniformTexLoc2 = glGetUniformLocation(shader, "sampler02");
        }


        /**
         * repaint the component if it's marked as dirty
         */
        if (isDirty) {

            glEnable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            isDirty = false;
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
            glPushAttrib(GL_VIEWPORT_BIT);
            glViewport(0, 0, size * 2, size * 2);

            glUseProgram(0);
            glClearColor(0,0,0,0);
            glClear(GL_COLOR_BUFFER_BIT);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(0, 1, 0, 1, -1, 1);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();




            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture);
            glColor3f(.2f, .2f, .2f);
            GLUtils.fillRectangleC(0, 1, 1, -1, 0);


            glUseProgram(shader);
            glUniform1f(uniformHue, hue);

            glUniform1i(uniformTexLoc, 0);
            glUniform1i(uniformTexLoc2, 1);

            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, textureHueAlpha);

            GLUtils.fillRectangleC(0, 1, 1, -1, 0);

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, 0);
            glUseProgram(0);


            glBindTexture(GL_TEXTURE_2D, textureDragIcon);
            glColor3f(0.1f, .1f, .1f);
            GLUtils.drawRectangle(0.025f, 0.025f, 0.1f, 0.1f);

            glBindTexture(GL_TEXTURE_2D, 0);

            float f = val * 0.7f + 0.2f;
            float rx = (float) (Math.cos(sat * Math.PI / 2)) * f;
            float ry = (float) (Math.sin(sat * Math.PI / 2)) * f;
            Color c = Color.getHSBColor(hue, sat, val);

            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glColor3f(1, 1, 1);
            GLUtils.drawRectangle(rx, ry, 0.02f, 0.02f);
            glColor3f(c.getRed() / 255f, c.getGreen() / 255f, c.getBlue() / 255f);
            GLUtils.drawRectangle(rx, ry, 0.016f, 0.016f);
            glPopAttrib();
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        }

        /**
         * reset OpenGL color to white
         */
        glColor3f(1, 1, 1);

        /**
         * setup rending matrix
         */
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, Window.getCanvas().getWidth(), 0, Window.getCanvas().getHeight(), -1, 1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();


        drawDroplocationOutlineWhileDragging(size);

        renderFrameBuffer(size);
    }

    private void renderFrameBuffer(int size) {

        glBindTexture(GL_TEXTURE_2D, fboTexture);

        Vector2f pos = getAbsolutePosition(this.pos);

        float rot = getRotation(this.pos);
        if (lastRot == 270 && rot == 0) {
            rotation -= 360;
        }
        if (lastRot == 0 && rot == 270) {
            rotation += 360;
        }
        this.lastRot = rot;

        rotation = rotation + (getRotation(this.pos) - rotation) * 0.2f;


        x = x + (pos.x - x) * 0.2f;
        y = y + (pos.y - y) * 0.2f;
        GLUtils.fillRectangleC(x, y, size, size, rotation);

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    private void drawDroplocationOutlineWhileDragging(int size) {
        if (this.pos == Position.DRAGGING) {
            Vector2f pos = getAbsolutePosition(getClosestPos(x, Window.getCanvas().getHeight() - y));
            float rot = getRotation(this.pos);
            glBindTexture(GL_TEXTURE_2D, textureOutline);
            GLUtils.drawRectangleCInverted(pos.x, pos.y, size, size, rot);
        }
    }

    /**
     * converts the position enum (which points to either corner of the screen) into an absolute cordinate in pixels
     *
     * @param pos enum containing the corner of the screen
     * @return vector containing the absolute coordinates
     */
    private Vector2f getAbsolutePosition(Position pos) {
        switch (pos) {
            case TOP_LEFT:
                return new Vector2f(0, Window.getCanvas().getHeight());
            case TOP_RIGHT:
                return new Vector2f(Window.getCanvas().getWidth(), Window.getCanvas().getHeight());
            case BOTTOM_LEFT:
                return new Vector2f(0, 0);
            case BOTTOM_RIGHT:
                return new Vector2f(Window.getCanvas().getWidth(), 0);
            case FLOATING:
                return new Vector2f(this.x, this.y);
            case DRAGGING:
                return new Vector2f(this.x, this.y);
            default:
                return null;
        }
    }

    /**
     * returns the rotation in degrees according to the corner of the screen
     * the framebuffer will be rotated around this angle when according to it's position
     *
     * @param pos enum containing the corner of the screen
     * @return degree of the component in angles
     */
    private float getRotation(Position pos) {
        switch (pos) {
            case FLOATING:
                return 45;
            case BOTTOM_LEFT:
                return 0;
            case BOTTOM_RIGHT:
                return 90;
            case TOP_RIGHT:
                return 180;
            case TOP_LEFT:
                return 270;
            case DRAGGING:
                return getRotation(getClosestPos(x, Window.getCanvas().getHeight() - y));
            default:
                return 0;
        }
    }

    /**
     * returns the position closest to a certain coordinate
     *
     * @param x
     * @param y
     * @return
     */
    private Position getClosestPos(float x, float y) {
        Position pos = null;
        boolean right = false;
        boolean top = false;
        if (x > Window.getCanvas().getWidth() / 2) {
            right = true;
        }
        if (y < Window.getCanvas().getHeight() / 2) {
            top = true;
        }

        if (!right & !top) {
            pos = Position.BOTTOM_LEFT;
        } else if (right & !top) {
            pos = Position.BOTTOM_RIGHT;
        } else if (!right & top) {
            pos = Position.TOP_LEFT;
        } else if (right & top) {
            pos = Position.TOP_RIGHT;
        }
        return pos;
    }

    /**
     * generates an fbo with an empty fbo texture
     *
     * @param size width/height of the fbo target in pixels
     */
    private void generateFBO(int size) {
        if (fbo != 0) {
            glDeleteFramebuffersEXT(fbo);
        }
        if (fboTexture != 0) {
            glDeleteTextures(fboTexture);
        }

        fboTexture = glGenTextures();
        fbo = glGenFramebuffersEXT();

        glBindTexture(GL_TEXTURE_2D, fboTexture);
        {
            GLUtils.setupDefaultTextureParams();

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0, GL_BGRA, GL_UNSIGNED_BYTE, (ByteBuffer) null);
        }
        glBindTexture(GL_TEXTURE_2D, 0);


        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
        glBindTexture(GL_TEXTURE_2D, fboTexture);

        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, fboTexture, 0);

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);


    }

    /**
     * transforms absolute coordinates to the component's local coordinate system and normalizes the values to a 0-1 range
     *
     * @param v
     * @return
     */
    Vector2f makeCoordinatesLocal(Vector2f v) {
        float x = v.x, y = v.y;
        switch (pos) {
            case BOTTOM_LEFT:
                x = x / lastSize;
                y = ((float) Window.getCanvas().getHeight() - y) / lastSize;
                break;
            case BOTTOM_RIGHT:
                x = (Window.getCanvas().getWidth() - x) / lastSize;
                y = (Window.getCanvas().getHeight() - y) / lastSize;
                break;

            case TOP_LEFT:
                x = x / lastSize;
                y = y / lastSize;
                break;
            case TOP_RIGHT:
                x = (Window.getCanvas().getWidth() - x) / lastSize;
                y = y / lastSize;
                break;
        }
        return new Vector2f(x, y);
    }


    @Override
    public void onPress(PButtonEvent event, float x, float y, float pressure) {

        Vector2f v = makeCoordinatesLocal(new Vector2f(x, y));
        if (withinDragButtonBounds(v)) {
            pos = Position.DRAGGING;
            mode = Mode.DRAGGING;
        } else if (withinHueSelectorBounds(v)) {
            mode = Mode.SELECTING_HUE;
            lastAngle = (float) Math.atan2(v.x, v.y);
        } else if (withinColorSelectorBounds(v)) {
            mode = Mode.SELECTING_COLOR;
            lastAngle = (float) Math.atan2(v.x, v.y);
        }
        super.onClick(event, x, y, pressure);
    }

    /**
     * checks if a position in the local coordinate system is within the brightness/saturation selection area
     *
     * @param v Vector containing the local position
     * @return wether or not the position is within the zone
     */
    private boolean withinColorSelectorBounds(Vector2f v) {
        float dist = v.length();
        return dist < 0.9 && dist > 0.2;
    }

    /**
     * checks if a position in the local coordinate system is within the hue selection area
     *
     * @param v Vector containing the local position
     * @return wether or not the position is within the zone
     */
    private boolean withinHueSelectorBounds(Vector2f v) {
        float dist = v.length();
        return dist > 0.92 && dist < 1;
    }

    /**
     * checks if a position in the local coordinate system is within the bounds for the grabbable button to drag the component
     *
     * @param v Vector containing the local position
     * @return wether or not the position is within the zone
     */
    private boolean withinDragButtonBounds(Vector2f v) {
        return v.x < 0.13 && v.y < 0.13;
    }

    @Override
    public void onRelease(PButtonEvent event, float x, float y, float pressure) {


        if (mode != null) {
            switch (mode) {
                case DRAGGING:
                    this.pos = getClosestPos(x, y);

                    break;
            }
        }
        mode = null;
    }

    @Override
    public void onDrag(PLevelEvent event, float x, float y, float lastX, float lastY, float pressure, float lastPressure) {
        Vector2f v = makeCoordinatesLocal(new Vector2f(x, y));

        if (mode != null) {
            switch (mode) {
                case DRAGGING:
                    this.x = x;
                    this.y = Window.getCanvas().getHeight() - y;
                    isDirty = true;
                    break;
                case SELECTING_HUE:
                    float angle = (float) Math.atan2(v.x, v.y);
                    if (this.pos == Position.BOTTOM_LEFT || this.pos == Position.TOP_RIGHT)
                        this.hue += (angle - lastAngle) / Math.PI * 2;
                    else
                        this.hue -= (angle - lastAngle) / Math.PI * 2;
                    lastAngle = angle;
                    isDirty = true;
                    ColorManager.setActiveColor(getColor());
                    break;
                case SELECTING_COLOR:
                    float a = (float) Math.atan2(v.x, v.y);
                    float l = v.length();

                    if (this.pos == Position.BOTTOM_LEFT || this.pos == Position.TOP_RIGHT)
                        setSaturation(1 - a * 2 / (float) Math.PI);
                    else
                        setSaturation(a * 2 / (float) Math.PI);
                    setValue((l - 0.2f) / 0.7f);

                    isDirty = true;
                    ColorManager.setActiveColor(getColor());
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * converts the HSL model to a RGB color
     * x, y, and z of the vector represent the r, g and b value of the color
     *
     * @return Color in RGB as vector
     */
    private Vector3f getColor() {
        Color c = Color.getHSBColor(hue, sat, val);
        return new Vector3f(c.getRed() / 255f, c.getGreen() / 255f, c.getBlue() / 255f);
    }

    /**
     * sets the brightness of the color
     *
     * @param val
     */
    public void setValue(float val) {
        if (val < 0)
            val = 0;
        if (val > 1)
            val = 1;
        this.val = val;
    }

    /**
     * changes the saturation of the color
     *
     * @param sat
     */
    public void setSaturation(float sat) {
        if (sat < 0)
            sat = 0;
        if (sat > 1)
            sat = 1;
        this.sat = sat;
    }


    @Override
    public boolean withinBounds(Vector2f v) {
        boolean res = false;
        Vector2f center = null;
        switch (pos) {
            case BOTTOM_LEFT:
                center = new Vector2f(0, Window.getCanvas().getHeight());
                break;
            case TOP_LEFT:
                center = new Vector2f();
                break;
            case BOTTOM_RIGHT:
                center = new Vector2f(Window.getCanvas().getWidth(), Window.getCanvas().getHeight());
                break;
            case TOP_RIGHT:
                center = new Vector2f(Window.getCanvas().getWidth(), 0);
        }
        if (center != null) {
            float distance = Vector2f.sub(center, v, null).length();
            if (distance < lastSize)
                res = true;
        }

        return res;
    }

    /**
     * which corner of the window the component is in, or if the component is currently being repositioned(dragged) by the user
     */
    enum Position {
        TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, FLOATING, DRAGGING
    }

    /**
     * all possible states the component can be in.
     * DRAGGING -> the component is currently being repositioned
     * SELECTING_HUE -> the user first pressed the mouse button in the area to select the hue
     * SELECTING_COLOR -> the user first pressed the mouse button in the area to select the Saturation/Brightness
     */
    enum Mode {
        DRAGGING,
        SELECTING_HUE,
        SELECTING_COLOR
    }


}