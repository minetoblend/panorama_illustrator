package gui;

import jpen.PButtonEvent;
import jpen.PLevelEvent;
import kernel.ColorManager;
import kernel.FBO;
import org.lwjgl.util.vector.Vector2f;
import tool.Tool;
import tool.ToolRegistry;
import util.GLUtils;
import util.ShaderUtils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import static org.lwjgl.opengl.EXTFramebufferObject.*;
import static org.lwjgl.opengl.EXTFramebufferObject.GL_FRAMEBUFFER_EXT;
import static org.lwjgl.opengl.EXTFramebufferObject.glBindFramebufferEXT;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL20.*;

public class ToolProperties extends GUIElement implements Observer {

    private int unPremultiplyShader;

    ToolProperties() {
        ColorManager.addChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                previewDirty = true;
            }
        });

        ToolRegistry.addChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                isDirty = true;
                previewDirty = true;
                setProperties(((Tool) evt.getNewValue()).getProperties());
            }
        });
    }

    private void setProperties(HashMap<String, GuiProperty> properties) {
        if (this.properties != null) {
            for (GuiProperty property : this.properties.values()) {
                property.deleteObserver(this);
            }
        }
        this.properties = properties;
        for (GuiProperty property : this.properties.values()) {
            property.addObserver(this);
        }

        isDirty = true;
    }

    HashMap<String, GuiProperty> properties;

    int shaderTransparentGrid;
    int uniformTransparentGridOffset;
    int extendedWidth = 200;
    int extendedHeight = 350;
    int iconWidth = 20;
    int iconHeight = 20;

    Mode mode = Mode.IDLE;

    float x = 20;
    int extended = 1;
    float extendedTransition = 1;
    float y = Window.getCanvas().getHeight() - getHeight() - 20;
    float width = 0;
    float height = 0;

    int fbo, fboTexture;
    int fboPreview, fboPreviewTexture;

    boolean isDirty = true, previewDirty = true;
    float scrollPosition = 0;

    boolean openNorth() {
        return y < Window.getCanvas().getHeight() / 2;
    }

    int frame = 0;

    @Override
    public void render() {

        frame++;

        if (fboPreview == 0) {
            generatePreviewFBO(extendedWidth, 80);
        }
        if (previewDirty && isExtended() && frame % 10 == 0) {
            isDirty = true;
            previewDirty = false;
            drawPreview();
        }

        {
            float width = getWidth();
            float height = getHeight();

            if ((int) width != (int) this.width || (int) height != (int) this.height) {
                generateFBO((int) width * 2, (int) height * 2);
                isDirty = true;
            }

            this.width = width;
            this.height = height;
        }
        if (isExtended() && extendedTransition < 1)
            extendedTransition += 0.125f;
        else if (!isExtended() && extendedTransition > 0)
            extendedTransition -= 0.125f;
        if (extendedTransition < 0)
            extendedTransition = 0;
        //extendedTransition += (extended - extendedTransition) * 0.1f;

        if (isDirty || true) {
            isDirty = false;
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
            glPushAttrib(GL_VIEWPORT_BIT);
            glViewport(0, 0, (int) width * 2, (int) height * 2);

            glUseProgram(0);
            glClearColor(0, 0, 0, 0);
            glClear(GL_COLOR_BUFFER_BIT);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(0, width, 0, height, -1, 1);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            glColor4f(.9f, .9f, .9f, .8f);
            GLUtils.drawRoundedRectangle(0, 0, width, height, 20 * (1 - getTransitionFactor()) + 5);

            float alpha = getTransitionFactor();

            if (extendedTransition > 0.05) {

                /**
                 * draw grab icon
                 */
                {
                    int grabBarWidth = 30;
                    int grabBarHeight = 20;
                    float x = width / 2 - grabBarWidth / 2;
                    float y = 13;

                    if (openNorth()) {
                        y += height - grabBarHeight;
                    }

                    glColor4f(.5f, .5f, .5f, alpha);

                    x += grabBarWidth / 2;

                    glLineWidth(3);

                    glBegin(GL_LINES);


                    glVertex2f(x + 4, y);
                    glVertex2f(x, y + 5);
                    glVertex2f(x, y + 5);
                    glVertex2f(x - 4, y);

                    y -= 3;

                    glVertex2f(x + 4, y);
                    glVertex2f(x, y - 5);
                    glVertex2f(x, y - 5);
                    glVertex2f(x - 4, y);


                    glEnd();
                }

                /**
                 * draw brush preview
                 */
                {
                    float width = this.width;
                    float height = 80;
                    float x = 0;
                    float y = this.height - 100;

                    float gridOffsetY = 0;
                    if (openNorth()) {
                        y = 20;
                    } else {
                        gridOffsetY -= this.height - iconHeight;
                    }

                    glUseProgram(getTransparentGridShader());
                    glUniform2f(uniformTransparentGridOffset, 0, gridOffsetY * 2f);


                    GLUtils.fillRectangleC(x, y, width, height, 0);

                    glUseProgram(0);

                    int count = 1000;


                    if (unPremultiplyShader == 0)
                        unPremultiplyShader = ShaderUtils.getShader("un-premultiply");
                    glColor4f(1, 1, 1, alpha);


                    glUseProgram(unPremultiplyShader);
                    glBindTexture(GL_TEXTURE_2D, fboPreviewTexture);
                    GLUtils.fillRectangleC(x, y, extendedWidth, height, 0);
                    glBindTexture(GL_TEXTURE_2D, 0);

                    glUseProgram(0);
                }
                /**
                 * draw properties
                 */
                {
                    float width = getPropertiesWidth();
                    float height = getPropertiesHeight();
                    float x = 0;
                    float y = getPropertiesY();

                    glColor4f(1, 1, 1, alpha * 0.8f);

                    GLUtils.fillRectangleC(x, y, width, height, 0);
                    {
                        drawProperties(x, y, width, height);


                        glMatrixMode(GL_PROJECTION);
                        glLoadIdentity();
                        glOrtho(0, this.width, 0, this.height, -100, 100);
                        glMatrixMode(GL_MODELVIEW);
                        glLoadIdentity();
                    }


                    /**
                     * draw scrollbar
                     */
                    glBindTexture(GL_TEXTURE_2D, 0);
                    glEnable(GL_BLEND);
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                    glDisable(GL_ALPHA_TEST);
                    glUseProgram(0);
                    glEnable(GL_TEXTURE_2D);


                    glColor4f(.9f, .9f, .9f, alpha * 0.95f);
                    GLUtils.drawRoundedRectangle(x + width - 8, y, 8, height, 4);

                    /**
                     * draw scrollbar thumb
                     */
                    {


                        float thumbSize = getThumbSize();

                        float thumbX = x + width - 8;
                        float thumbY = getThumbY();


                        glColor4f(.5f, .5f, .5f, alpha * 0.8f);
                        GLUtils.drawRoundedRectangle(thumbX, thumbY, 8, thumbSize, 4);


                    }
                }
            }

            glPopAttrib();
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        }


        /**
         * reset OpenGL color to white
         */
        glColor3f(1, 1, 1);


        /**
         * setup rending matrix
         */
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, Window.getCanvas().getWidth(), 0, Window.getCanvas().getHeight(), -1, 1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();


        renderFrameBuffer((int) width, (int) height);
    }

    private void drawProperties(float x, float y, float width, float height) {
        glPushAttrib(GL_VIEWPORT_BIT);

        glViewport((int) x * 2, (int) y * 2, (int) width * 2, (int) height * 2);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(x, x + width, y + height, y, -1, 1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();


        y += height - ((1 - scrollPosition) * (getPropertiesHeight() - totalPropertiesHeight()));

        float offsetY = 0;

        for (int i = properties.size() - 1; i >= 0; i--) {
            GuiProperty value = (GuiProperty) properties.values().toArray()[i];
            offsetY -= value.element.getHeight();
            value.element.render(x, y + offsetY, width, value.element.getHeight());

        }

        glViewport(0, 0, (int) width * 2, (int) height * 2);

        glPopAttrib();
    }

    private void drawPreview() {

        glPushAttrib(GL_VIEWPORT_BIT);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboPreview);

        glViewport(0, 0, extendedWidth, 80);

        glClearColor(0, 0, 0, 0);
        glClear(GL_COLOR_BUFFER_BIT);

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        glPopAttrib();

        glPushAttrib(GL_VIEWPORT_BIT);


        glColor3f(0, 0, 0);

        for (int i = 0; i < 10; i++) {
            float f = i / 10f;
            float f2 = (i + 1) / 10f;

            //System.out.println("brush");

            InputDragEvent event = new InputDragEvent(
                    new CursorInputData(20 + (extendedWidth - 40) * f, 40 + (float) Math.sin(f * 2 * Math.PI) * 15, 1 - ((f * 2 - 1) * (f * 2 - 1)), null),
                    new CursorInputData(20 + (extendedWidth - 40) * f2, 40 + (float) Math.sin(f2 * 2 * Math.PI) * 15, 1 - ((f2 * 2 - 1) * (f2 * 2 - 1)), null)
            );

            //System.out.println(20 + (extendedWidth - 40) * f + "  " + (40 + (float) Math.sin(f * 2 * Math.PI)));

            ToolRegistry.getCurrentTool().onDragPreview(event, new FBO(fboPreview, fboPreviewTexture));

        }

        glPopAttrib();

    }

    private int getTransparentGridShader() {
        if (shaderTransparentGrid == 0) {
            shaderTransparentGrid = ShaderUtils.getShader("transparentGrid");
            uniformTransparentGridOffset = glGetUniformLocation(shaderTransparentGrid, "offset");
        }

        return shaderTransparentGrid;
    }

    private float getThumbY() {
        float height = getPropertiesHeight(), y = getPropertiesY(), thumbSize = getThumbSize();

        return y + (height - thumbSize) * (1 - scrollPosition);
    }

    private float getPropertiesY() {
        float y = this.height - 100 - getPropertiesHeight();

        if (openNorth())
            y = 100;
        return y;
    }

    private float getPropertiesWidth() {
        return this.width;
    }

    private float getPropertiesHeight() {
        return this.height - 120;
    }

    private float getThumbSize() {


        float height = getPropertiesHeight();

        float thumbSize = height * height / totalPropertiesHeight();

        if (thumbSize > height)
            thumbSize = height;

        return thumbSize;
    }

    boolean withinHeightDraggingButtonArea(Vector2f v) {

        float y = 0;
        float height = 20;

        if (openNorth()) {
            y += this.height - 20;
        }


        return v.x >= 0 && v.x < getWidth() &&
                v.y >= y && v.y < y + height;
    }

    private Vector2f mapToLocalCoordinates(Vector2f v) {
        return new Vector2f(v.x - x, Window.getCanvas().getHeight() - v.y - getAbsoluteY());
    }

    private float getTransitionFactor() {
        return 1 - (float) (Math.cos(extendedTransition * Math.PI) * 0.5 + 0.5);
    }

    boolean isExtended() {
        return extended == 1;

    }

    private int getHeight() {
        float f = 1 - (float) (Math.cos(extendedTransition * Math.PI) * 0.5 + 0.5);

        return (int) (iconHeight + (extendedHeight - iconHeight) * f);
    }

    private int getWidth() {
        float f = 1 - (float) (Math.cos(extendedTransition * Math.PI) * 0.5 + 0.5);
        return (int) (iconWidth + (extendedWidth - iconWidth) * f);
    }

    @Override
    public void onDrag(PLevelEvent event, float x, float y, float lastX, float lastY, float pressure, float lastPressure) {
        Vector2f v = mapToLocalCoordinates(new Vector2f(x, y));
        Vector2f v2 = mapToLocalCoordinates(new Vector2f(lastX, lastY));
        switch (mode) {
            case IDLE:
                break;
            case CHANGING_HEIGHT:
                if (openNorth())
                    setExtendedHeight((int) (extendedHeight - y + lastY));
                else
                    setExtendedHeight((int) (extendedHeight + y - lastY));
                break;
            case PROPERTY:

                float oX = propertyXOffset;
                float oY = propertyYOffset;
                currentProperty.element.onDrag(event,
                        v.x - oX, currentProperty.element.getHeight() - (v.y - oY),
                        v2.x - oX, currentProperty.element.getHeight() - (v2.y - oY),
                        pressure, lastPressure);
                break;

            case SCROLLING:
                scroll(y - lastY);
                break;
        }
    }

    private void scroll(float pixels) {

        if (getPropertiesHeight() != getThumbSize())
            scrollPosition += pixels / (getPropertiesHeight() - getThumbSize());

        if (scrollPosition < 0) scrollPosition = 0;
        if (scrollPosition > 1) scrollPosition = 1;

        isDirty = true;
    }

    public void setExtendedHeight(int newVal) {
        if (newVal < 200)
            newVal = 200;
        if (newVal > 500)
            newVal = 500;
        this.extendedHeight = newVal;

        isDirty = true;
    }

    @Override
    public void onRelease(PButtonEvent event, float x, float y, float pressure) {
        if (currentProperty != null) {
            Vector2f v = mapToLocalCoordinates(new Vector2f(x, y));
            float oX = propertyXOffset;
            float oY = propertyYOffset;
            currentProperty.element.onRelease(event, v.x - oX, currentProperty.element.getHeight() - (v.y - oY), pressure);
            currentProperty = null;
        }
        this.mode = Mode.IDLE;
    }

    @Override
    public void onPress(PButtonEvent event, float x, float y, float pressure) {

        Vector2f v = mapToLocalCoordinates(new Vector2f(x, y));

        if (isExtended()) {
            if (withinHeightDraggingButtonArea(v)) {
                mode = Mode.CHANGING_HEIGHT;
            } else if (withinScollThumbArea(v)) {
                mode = Mode.SCROLLING;
            } else if (withinPropertiesArea(v)) {


                float oX = 0;
                float oY = getPropertiesY();
                oY += getPropertiesHeight() - (scrollPosition * (getPropertiesHeight() - totalPropertiesHeight()));


                GuiProperty[] values = new GuiProperty[properties.size()];
                properties.values().toArray(values);

                for (int i = 0; i < properties.size(); i++) {
                    GuiProperty property = values[i];
                    oY -= property.element.getHeight();

                    if (v.x >= oX && v.y >= oY && v.x < oX + getPropertiesWidth() && v.y < oY + property.element.getHeight()) {
                        mode = Mode.PROPERTY;
                        currentProperty = property;
                        propertyXOffset = oX;
                        propertyYOffset = oY;
                        property.element.onPress(event, v.x - oX, property.element.getHeight() - (v.y - oY), pressure);
                        return;
                    }
                }
            } else
                extended = extended == 1 ? 0 : 1;
        } else
            extended = extended == 1 ? 0 : 1;
    }

    GuiProperty currentProperty;
    float propertyXOffset;
    float propertyYOffset;

    private boolean withinPropertiesArea(Vector2f v) {
        return v.x >= 0 && v.x < getPropertiesWidth() &&
                v.y >= getPropertiesY() && v.y < getPropertiesY() + getPropertiesHeight();

    }

    private boolean withinScollThumbArea(Vector2f v) {


        float x = getPropertiesWidth() - 8;
        float y = getThumbY();
        float height = getThumbSize();
        float width = 8;

        return v.x >= x && v.x < x + width && v.y >= y && v.y < y + height;
    }

    @Override
    public boolean withinBounds(Vector2f v) {
        v.y = Window.getCanvas().getHeight() - v.y;
        if (!isExtended()) {
            Vector2f center = new Vector2f(x + iconWidth / 2, y + iconHeight / 2);
            float distance = Vector2f.sub(v, center, null).length();
            if (distance < iconHeight / 2 && distance < iconWidth / 2) {
                return true;
            }

        } else {
            float x = getAbsoluteX();
            float y = getAbsoluteY();

            return
                    v.x >= x &&
                            v.y >= y &&
                            v.x < x + width &&
                            v.y < y + height;
        }
        return false;
    }

    private void renderFrameBuffer(int width, int height) {

        glBindTexture(GL_TEXTURE_2D, fboTexture);

        float x = getAbsoluteX();
        float y = getAbsoluteY();

        GLUtils.fillRectangleC(x, y, width, height, 0);

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    private float getAbsoluteX() {
        float x = this.x;

        if (!openEast()) {
            x -= width - iconWidth;
        }
        return x;
    }

    private float getAbsoluteY() {
        float y = this.y;


        if (!openNorth()) {
            y -= height - iconHeight;
        }
        return y;
    }

    private boolean openEast() {
        return x < Window.getCanvas().getHeight() / 2;
    }

    /**
     * generates an fbo with an empty fbo texture
     *
     * @param width  width of the fbo target in pixels
     * @param height height of the fbo target in pixels
     */
    private void generateFBO(int width, int height) {
        if (fbo != 0) {
            glDeleteFramebuffersEXT(fbo);
        }
        if (fboTexture != 0) {
            glDeleteTextures(fboTexture);
        }


        fboTexture = glGenTextures();
        fbo = glGenFramebuffersEXT();

        glBindTexture(GL_TEXTURE_2D, fboTexture);
        {
            GLUtils.setupDefaultTextureParams();

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, (ByteBuffer) null);
        }
        glBindTexture(GL_TEXTURE_2D, 0);


        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
        glBindTexture(GL_TEXTURE_2D, fboTexture);

        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, fboTexture, 0);

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);


    }

    /**
     * generates an fbo with an empty fbo texture
     *
     * @param width  width of the fbo target in pixels
     * @param height height of the fbo target in pixels
     */
    private void generatePreviewFBO(int width, int height) {
        if (fboPreview != 0) {
            glDeleteFramebuffersEXT(fboPreview);
        }
        if (fboPreviewTexture != 0) {
            glDeleteTextures(fboPreviewTexture);
        }


        fboPreviewTexture = glGenTextures();
        fboPreview = glGenFramebuffersEXT();

        glBindTexture(GL_TEXTURE_2D, fboPreviewTexture);
        {
            GLUtils.setupDefaultTextureParams();

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, (ByteBuffer) null);
        }
        glBindTexture(GL_TEXTURE_2D, 0);


        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboPreview);
        glBindTexture(GL_TEXTURE_2D, fboPreviewTexture);

        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, fboPreviewTexture, 0);

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);


    }


    @Override
    public void update(Observable o, Object val) {
        previewDirty = true;
    }

    enum Mode {
        IDLE,
        CHANGING_HEIGHT,
        SCROLLING,
        PROPERTY;
    }

    private float totalPropertiesHeight() {
        float height = 0;
        if (properties != null)
            for (GuiProperty property : properties.values()) {
                height += property.element.getHeight();
            }
        return height;
    }
}
