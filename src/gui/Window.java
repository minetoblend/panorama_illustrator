package gui;


import kernel.Project;
import sun.swing.MenuItemCheckIconFactory;

import javax.swing.*;
import java.awt.*;

public class Window {

    private static JFrame frame;

    private static Canvas canvas;

    static SplashScreen splashScreen = new SplashScreen();

    public static void init() {



        initWindow();


    }

    private static void initCanvas() {
        canvas = Canvas.getInstance();

        frame.add(canvas);
    }

    private static void initWindow() {

        frame = new JFrame();
        frame.setLayout(new BorderLayout());
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);




        initCanvas();

        JMenuBar menuBar = new JMenuBar();

        {
            JMenu menuFile = new JMenu("File");
            {
                JMenuItem menuFileNew = new JMenuItem("New");
                menuFileNew.addActionListener(event->{
                    Project.createNew();
                });
                menuFile.add(menuFileNew);
            }
            menuBar.add(menuFile);
        }

        frame.add(menuBar, BorderLayout.NORTH);


        frame.setVisible(true);




    }

    public static void render() {
        canvas.repaint();
    }

    public static Canvas getCanvas() {
        return canvas;
    }

    public static JFrame getFrame() {
        return frame;
    }
}
