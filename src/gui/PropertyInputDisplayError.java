package gui;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import util.GLUtils;

import java.awt.*;

import static org.lwjgl.opengl.GL11.glColor4f;

public class PropertyInputDisplayError extends PropertyInput {

    UnicodeFont font;
    String message;

    public PropertyInputDisplayError(String message) {
        this.message = message;

        font = new UnicodeFont(new Font(Font.SANS_SERIF, 0, 15));
        font.addAsciiGlyphs();
        font.getEffects().add(new ColorEffect(Color.BLACK));
        try {
            font.loadGlyphs();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void render(float x, float y, float w, float h) {
        glColor4f(1, 0, 0, 0.5f);
        GLUtils.fillRectangleC(x, y, w, h, 0);

        font.drawString(x + 10, y, message);
    }

    @Override
    public int getHeight() {
        return 20;
    }
}
