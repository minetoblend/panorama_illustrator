package gui;

import resources.images.ImageLocator;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class SplashScreen extends JWindow {

    BufferedImage img;

    SplashScreen(){
        System.out.println("test");
        try {
            img = ImageIO.read(ImageLocator.class.getResourceAsStream("splash_screen.png"));
            setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
            setAlwaysOnTop(true);
            setSize(img.getWidth(), img.getHeight());
            setLocationRelativeTo(null);
            showSplashScreen(true);
            //setAlwaysOnTop(true);
            setBackground(new Color(0, 255, 0, 0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showSplashScreen(boolean b) {
        System.out.println("showing splashscreen = " + b);
        setVisible(b);
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }
}
