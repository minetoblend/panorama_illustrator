package gui;

import jdk.nashorn.api.scripting.ScriptObjectMirror;
import jdk.nashorn.internal.objects.NativeObject;
import kernel.Curve;
import java.util.Observable;

public class GuiProperty<T> extends Observable {

    T value;

    String name;
    String type;

    double min;
    double max;
    boolean minimized = true;
    boolean activated;
    Curve curve;

    public GuiProperty(ScriptObjectMirror o) {


        this.name = (String) o.get("name");
        this.type = (String) o.get("type");
        this.value = (T) o.get("value");


        if (o.containsKey("min")) {
            min = new Double(o.get("min").toString());

        } else {
            min = Float.MIN_VALUE;
        }

        if (o.containsKey("max")) {
            max = new Double(o.get("max").toString());
        } else {
            max = Float.MAX_VALUE;
        }

        if (o.containsKey("minimized"))
            this.minimized = (boolean) o.get("minimized");

        if (o.containsKey("activated"))
            this.activated = (boolean) o.get("activated");


        switch (type) {
            case "float":
                element = new PropertyInputFloat((GuiProperty<Double>) this);
                break;
            case "curve":
                element = new PropertyInputCurve(this);
                curve = new Curve((String) value);
                break;
            default:
                element = new PropertyInputDisplayError("Unknown Type: '" + type + "'");
        }
    }

    PropertyInput element;

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(T newVal) {

        this.value = newVal;
        setChanged();
        notifyObservers(newVal);
    }

    public String getType() {
        return type;
    }

    public void setActivated(boolean b) {

        this.activated = b;
        this.minimized = !b;
        setChanged();
        notifyObservers(b);
    }

    public void updated() {
        setChanged();
        notifyObservers(null);
    }

    public Curve getCurve() {
        return curve;
    }

    public float evaluate(float pos) {
        if (activated) return curve.evaluate(pos);
        return 1;
    }
}