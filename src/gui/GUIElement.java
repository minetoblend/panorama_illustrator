package gui;

import jpen.PButtonEvent;
import jpen.PLevelEvent;
import org.lwjgl.util.vector.Vector2f;

/**
 * Superclass for all GUI Elements
 */
public abstract class GUIElement {

    public void render(){

    }

    public void render(float x, float y, float w, float h){

    }

    public void onClick(PButtonEvent event, float x, float y, float pressure){

    }

    public void onDrag(PLevelEvent event, float x, float y, float lastX, float lastY, float pressure, float lastPressure){

    }

    public void onRelease(PButtonEvent event, float x, float y, float pressure){

    }

    public void onPress(PButtonEvent event, float x, float y, float pressure){

    }

    public boolean withinBounds(Vector2f v){
        return false;
    }
}
