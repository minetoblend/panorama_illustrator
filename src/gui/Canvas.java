package gui;

import jpen.owner.multiAwt.AwtPenToolkit;
import kernel.Program;
import kernel.Project;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.AWTGLCanvas;
import tool.ToolRegistry;
import util.GLUtils;
import util.ShaderUtils;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;

import static org.lwjgl.opengl.EXTFramebufferObject.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL12.GL_BGRA;


public class Canvas extends AWTGLCanvas {

    private static Canvas instance;
    private static Object mutex = new Object();

    public static Canvas getInstance() {
         Canvas result = instance;
        if (result == null) {
            synchronized (mutex) {
                result = instance;
                if (result == null) {
                    try {
                        instance = result = new Canvas();
                    } catch (LWJGLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return result;
    }

    private boolean resized = true;
    ArrayList<GUIElement> gui;

    private Canvas() throws LWJGLException {
        super();
        CanvasInputHandler handler = new CanvasInputHandler();
        AwtPenToolkit.addPenListener(this, handler);
        addMouseWheelListener(handler);
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                resized = true;
            }
        });

        gui = new ArrayList<>();
    }

    @Override
    protected void initGL() {
        glClearColor(0, 0, 0, 0);
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        setVSyncEnabled(true);
        glEnable(GL_FRAMEBUFFER_EXT);

        Program.setProject(new Project(2048, 1024));

        initGui();

        ToolRegistry.init();

        Window.splashScreen.showSplashScreen(false);
        Window.getFrame().setVisible(true);
        Window.getFrame().requestFocus();
    }

    private void initGui() {
        gui.add(new ColorPicker());
        gui.add(new ToolProperties());
    }



    /*
     * performs all drawing on the screen
     */
    @Override
    protected void paintGL() {
        try {
            long time = System.currentTimeMillis();
            if (Program.getProject() != null)
                Program.getProject().render();
            //System.out.println("project render time: " + (System.currentTimeMillis() - time));
            time = System.currentTimeMillis();
            renderGui();
            //System.out.println("gui render time: " + (System.currentTimeMillis() - time));
            time = System.currentTimeMillis();
            swapBuffers();
            //System.out.println("swap buffer time: " + (System.currentTimeMillis() - time));

            if(Math.random() > 0.99)
                System.gc();

        } catch (LWJGLException e) {
            e.printStackTrace();
        }
    }

    private void renderGui() {

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        /**
         * Sets up the projection matrix for all drawing done by OpenGL.
         */
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, getWidth(), 0, getHeight(), -1, 1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        for (GUIElement e : gui) {
            e.render();
        }

    }

    public float getAspectRatio() {
        return (float) getWidth() / getHeight();
    }

    public boolean getResized() {
        return resized;
    }

    public void setResized(boolean resized) {
        this.resized = resized;
    }

    public ArrayList<GUIElement> getGui() {
        return gui;
    }
}
