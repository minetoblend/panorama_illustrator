package layer;

import kernel.FBO;
import kernel.Project;
import org.lwjgl.BufferUtils;
import resources.images.ImageLocator;
import util.GLUtils;
import util.ShaderUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_RGBA16F;

public class BitmapLayer extends ProjectLayer {


    private int glTexture;
    private int width, height;

    private FBO fbo;

    public BitmapLayer(Project project, int width, int height) {
        super(project);
        this.width = width;
        this.height = height;
        this.glTexture = createEmptyGLTexture();
        System.out.println("Created BitmapLayer '" + getName() + "' with Texture ID: " + glTexture);
    }

    public BitmapLayer(Project project, String name, int width, int height) {
        super(project, name);
        this.width = width;
        this.height = height;
        this.glTexture = createEmptyGLTexture();

        System.out.println("Created BitmapLayer '" + getName() + "' with Texture ID: " + glTexture);
    }

    private int createEmptyGLTexture() {
        int glTexture = glGenTextures();

        ByteBuffer buf = null;
        //int width = this.width;
        //int height = this.height;
        try {

            BufferedImage img = ImageIO.read(ImageLocator.class.getResourceAsStream("Grid.jpg"));
            this.width = img.getWidth();
            this.height = img.getHeight();
            buf = getBufferFromImage(img);
        } catch (IOException e) {
            e.printStackTrace();
        }


        glBindTexture(GL_TEXTURE_2D, glTexture);
        {
            //Setup filtering, i.e. how OpenGL will interpolate the pixels when scaling up or down
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            //Setup wrap mode, i.e. how OpenGL will handle pixels outside of the expected range
            //Note: GL_CLAMP_TO_EDGE is part of GL12
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, (ByteBuffer) buf);
        }
        glBindTexture(GL_TEXTURE_2D, 0);
        return glTexture;
    }

    @Override
    public void render() {

        glBlendFunc(blendingMode.getSrcFac(), blendingMode.getDestFac());

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        glOrtho(-1, 1, -1, 1, -1, 1);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glColor3f(1,1,1);

        glBindTexture(GL_TEXTURE_2D, getGLTexture());
        glUseProgram(Project.getProjectionShader());

        GLUtils.drawRectangle();

        glUseProgram(0);
        glBindTexture(GL_TEXTURE_2D, 0);


        super.render();
    }

    private int getGLTexture() {
        if (glTexture == 0) {
            //TODO: create texture when none is presenet yet
        }
        return glTexture;
    }

    /**
     * converts a {@link BufferedImage} into a {@link ByteBuffer}
     *
     * @param image {@link BufferedImage} containing the image data
     * @return {@link ByteBuffer} containing the image data
     */
    private ByteBuffer getBufferFromImage(BufferedImage image) {
        ByteBuffer textureData = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * Integer.SIZE);

        int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

        for (int i : pixels) {
            textureData.putInt(i);
        }
        textureData.flip();
        return textureData;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public FBO getFbo() {
        if(fbo == null)
            fbo = new FBO(getGLTexture());
        return fbo;
    }
}