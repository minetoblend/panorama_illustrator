package layer;

import blending.BlendingMode;
import blending.BlendingModeRegistry;
import kernel.FBO;
import kernel.Project;

/**
 * Superclass for all project layers.
 * Project layers contain all the image data
 * They get drawn back to front onto the application's main frame buffer
 */
public class ProjectLayer {
    /**
     * The project the layer belongs to
     */
    private Project project;

    /**
     * the name of the layer
     */
    private String name;

    /**
     * how the layer gets combined with the layers below it
     * @see <a href="https://helpx.adobe.com/uk/photoshop/using/blending-modes.html">https://helpx.adobe.com/uk/photoshop/using/blending-modes.html</a>
     */
    BlendingMode blendingMode = BlendingModeRegistry.get("normal");


    public ProjectLayer(Project project){
        this(project, project.getNextLayerName());
    }


    public ProjectLayer(Project project, String name) {
        this.project = project;
        this.name = name;
    }

    /**
     *  gets called to draw the layer
     */
    public void render(){

    }

    public Project getProject() {
        return project;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BlendingMode getBlendingMode() {
        return blendingMode;
    }

    public FBO getFbo() {
        return null;
    }
}