package layer;

import kernel.Project;
import org.lwjgl.util.vector.Vector3f;
import util.ColorUtils;

import java.awt.Color;

import static org.lwjgl.opengl.GL11.*;

public class BackgroundLayer {


    boolean enabled;

    Vector3f color;


    public BackgroundLayer(Vector3f color) {
        this.color = color;
        enabled = true;
    }

    public BackgroundLayer(Color c) {
        this(ColorUtils.ColortoVector3f(c));
    }


    /**
     * simply clears the buffer and fills it with the background color
     */
    public void render() {
        if (enabled) {
            glClearColor(color.x, color.y, color.z, 1f);
            glClear(GL_COLOR_BUFFER_BIT);        }
    }
}
