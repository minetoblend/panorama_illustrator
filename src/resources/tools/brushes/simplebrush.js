var name = "Simple brush";
//brush size in pixels


//pointer to the shader program
var shader = ShaderUtils.getShader("lineTool2");

//pointers to the variables passed down to the shader
var uniformCol = GL20.glGetUniformLocation(shader,"col");
var uniformAlpha = GL20.glGetUniformLocation(shader,"alpha")
var uniformHardness = GL20.glGetUniformLocation(shader,"hardness")

var properties = [
    {
            name: "size",
            type: "float",
            display: "slider",
            min: 0,
            max: 1000,
            value: 10.0
    },
    {
        name: "pressure",
        type: "curve",
        activated: true,
        value: "linear"
    },
    {
        name: "opacity",
        type: "float",
        display: "slider",
        min: 0,
        max: 1,
        value: 1.0
    },
    {
        name: "hardness",
        type: "float",
        display: "slider",
        min: 0,
        max: 1,
        value: 0.0
    }
]

function onDrag(event){



    fbo.bind();

    allPoints(event,function (coords, pressure){

        GL20.glUseProgram(shader);

        gl.glBlendFunc(gl.GL_ONE, gl.GL_ONE_MINUS_SRC_ALPHA)

        GL20.glUniform3f(uniformCol,activeColor.x,activeColor.y,activeColor.z)
        GL20.glUniform1f(uniformAlpha, getProperty("opacity"))
        GL20.glUniform1f(uniformHardness, getProperty("hardness"))



        drawCenteredSquare(coords, getProperty("size")*getCurve("pressure").evaluate(pressure));

        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

        GL20.glUseProgram(0);
    });

    fbo.unbind();
}

function onRelease(event, layer){

}