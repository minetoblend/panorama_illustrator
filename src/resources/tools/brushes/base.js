
load("nashorn:mozilla_compat.js")

importPackage(Packages.org.lwjgl.util.vector)
importPackage(Packages.kernel)
importPackage(Packages.util)
importPackage(Packages.layer)
importPackage(Packages.gui)
importPackage(org.lwjgl.opengl)
importPackage(java.lang)
importPackage(java.util)

var gl = GL11


var rotX, rotZ, zoom, aspectRatio, layer, projected = true;


//interpolates between the start and end position of an event and calls the callback function with the given input details(mouse position, pressure, etc.) for each point
function allPoints(event, callback, pointDensity){
    if(pointDensity == undefined){
        pointDensity = 1
    }

    var p1 = new Vector2f(event.p1.x,event.p1.y);
    var p2 = new Vector2f(event.p2.x,event.p2.y);



    if(projected){
        var p1Projected = project(normalizeDisplayVector(p1)).point;
        var p2Projected = project(normalizeDisplayVector(p2)).point;

        p1Projected.x *= 2;
        p2Projected.x *= 2;

        var distance = Vector2f.sub(p1,p2, null).length();

        var points = distance / pointDensity;
        if(points < 1)
            points = 1;



        for(var i = 0; i < points; i++){
            var f = i / points;
            var point = mixVectors(p1,p2,f);
            var pressure = mix(event.p1.pressure,event.p2.pressure, f);

            projectedPoint = project(normalizeDisplayVector(point));
            point = projectedPoint.point;
            var scaled = new Vector2f(point.x*width, point.y*height);
            callback(scaled, pressure);

        }
    }
    else{
            var distance = Vector2f.sub(p1,p2, null).length();



            var points = distance / pointDensity;
            if(points < 1)
                points = 1;


            for(var i = 0; i < points; i++){
                var f = i / points;

                var point = mixVectors(p1,p2,f);
                var pressure = mix(event.p1.pressure,event.p2.pressure, f);

                callback(point, pressure);
            }
        }
}

//normalizes a vector to the range of [-1, 1] along the x and y axis
function normalizeDisplayVector(v){
    var w = getCanvasWidth();
    var h = getCanvasHeight();
    var x = v.x / w * 2 - 1;
    var y = v.y / h * 2 - 1;

    return new Vector2f(x,y);;
}



//projects a vector from euclidian(xyz) to equirectangular(longitude, latitude) projection
function project(point){
    if(Program.getProject().displayMode == Project.DisplayMode.PROJECTED){
        var x = point.x / zoom * aspectRatio;
        var y = point.y / zoom;

        var dir = new Vector3f(x,1,y).normalise(null);

        dir = rotateX(dir, rotX);
        dir = rotateZ(dir, rotZ);

        x = -Math.atan2(dir.y, dir.x) / (Math.PI*2.0) + 0.5;
        x = -Math.atan2(dir.getY(), dir.getX()) / (Math.PI * 2.0) + 0.5;

        y = Math.atan2(dir.z, new Vector2f(dir.x,dir.y).length()) / Math.PI + 0.5;

        return {point: new Vector2f(x, y)};

    }else if(Program.getProject().displayMode == Project.DisplayMode.FLAT){

    }
    return {point:point};
}


//roates a 3d vector around the x axis
function rotateX(v, angle){
    var c = Math.cos(angle);
    var s = Math.sin(angle);
    return new Vector3f(
            v.x,
            v.y * c - v.z * s,
            v.y * s + v.z * c
        );
}

//roates a 3d vector around the z axis
function rotateZ(v, angle){
    var c = Math.cos(angle);
    var s = Math.sin(angle);
    return new Vector3f(
            v.x * c - v.y * s,
            v.x * s + v.y * c,
            v.z
        );
}

// linearlz interpolates between two vectors based on an interpolation factor[0, 1] -> vectors will be combined a + (b - a) * f
function mixVectors(v1, v2, f) {
    return new Vector2f(mix(v1.x, v2.x, f), mix(v1.y, v2.y, f));
}

// blends two values together based on a mix factor determining to what degree the second value overrides the first
function mix(a, b, f) {
    return a + (b - a) * f;
}

function getCanvasWidth(){
    return Window.getCanvas().getWidth();
}

function getCanvasHeight(){
    return Window.getCanvas().getHeight();
}



//sets up the default projection matrix
function setupDefaultMatrix(){
    gl.glPushAttrib(gl.GL_VIEWPORT_BIT)
    gl.glViewport(0, 0, width, height);

    gl.glMatrixMode(gl.GL_PROJECTION);
    gl.glLoadIdentity();
    gl.glOrtho(0, width, 0, height, -1, 1);
    gl.glMatrixMode(gl.GL_MODELVIEW);
    gl.glLoadIdentity();
}

//changes the OpenGL color to a certain value using r, g, b and a values
//can be called as (r,g,b,a) or as (r,g,b)
function setColor(r,g,b,a){
    if(a == undefined){
        gl.glColor3f(r,g,b);
    }else{
        gl.glColor4f(r,g,b,a);
    }
}

//changes the OpenGL color to a certain value using a vector
function setColor(col){
    gl.glColor4f(col.x,col.y,col.z, 0.2);
}

//draws a square positioned by it's center
function drawCenteredSquare(pos, size){
    GLUtils.drawRectangle(pos.x,pos.y,size,size);
}

function drawCenteredRectangle(pos, width, height, angle){
    if(angle == undefined) angle = 0;
    GLUtils.drawRectangle(pos.x,pos.y,width, height, angle);

}

//resets all OpenGL states to default value
function init(){
    GL20.glUseProgram(0);
    gl.glBindTexture(gl.GL_TEXTURE_2D, 0);
    setupDefaultMatrix();
}

//
function cleanup(){

    GL20.glUseProgram(0);
    gl.glBindTexture(gl.GL_TEXTURE_2D, 0);
    gl.glPopAttrib();
}

function initProperties(){
    if(properties){
        for(var i = 0; i < properties.length; i++){
            var property = new GuiProperty(properties[i]);
            propertyValues.put(property.name, property);

        }
    }

}

function getProperty(name){
    return propertyValues.get(name).getValue()
}

function getCurve(name){
    return propertyValues.get(name);
}