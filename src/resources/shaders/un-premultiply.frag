uniform sampler2D sampler;
varying vec4 vertColor;

void main() {
    vec4 col = texture2D(sampler, gl_TexCoord[0].st);
    if(col.a != 0){
        col.rgb /= col.a;
    }
    col.a *= vertColor.a;
    gl_FragColor = col;
}
