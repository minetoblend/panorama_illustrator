in vec4 gl_FragCoord;

uniform sampler2D tex;
uniform float rotX;
uniform float rotZ;
uniform float zoom;
uniform float aspectRatio;


mat3 rotationMatrixX(float angle);
mat3 rotationMatrixZ(float angle);
mat4 rotationMatrix(vec3 axis, float angle);

const float pi = 3.14159265359;

void main(){

	//texture coordinates mapped to [-1, 1] and stretched according to the screen aspectRatio
	float u = gl_TexCoord[0].s * zoom * aspectRatio;
	float v = -gl_TexCoord[0].t * zoom;

    //creates a normalized vector poiting towards the direction of the current pixel
	vec4 dir = normalize(vec4(u,1,v,1));
	dir *= rotationMatrix(vec3(1,0,0),rotX);
    dir *= rotationMatrix(vec3(0,0,1),rotZ);

    //converts the planar coordinates to spherical coordiantes
	float x = -atan(dir.y, dir.x) / (pi*2.0) + 0.5;
	float y = atan(dir.z, length(dir.xy)) / pi + 0.5;

	//farbe aus der textur holen
	gl_FragColor = texture2D(tex,vec2(x,y));
}

// creates a rotation matrix to rotate a vector about the X axis by a certain angle
// => x * matrix = x rotated around the x axis by angle
mat3 rotationMatrixX(float angle){
   float s = sin(angle);
   float c = cos(angle);
   return mat3(1,0,0,
                0,c,-s,
                0,s,c);
}

// creates a rotation matrix to rotate a vector about the Y axis by a certain angle
// => y * matrix = y rotated around the y axis by angle
mat3 rotationMatrixZ(float angle){
   float s = sin(angle);
   float c = cos(angle);
   return mat3(c,-s,0,
   s,c,0,
   0,0,1);
}

//creates a rotation matrix to rotate a vector around another vector
mat4 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}