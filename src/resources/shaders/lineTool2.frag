
uniform vec3 col;
uniform float alpha;
uniform float hardness;

vec3 rgb2hsv(vec3 c);
vec3 hsv2rgb(vec3 c);

void main() {
	float maxAlpha = 0.0;
	float dist = distance(vec2(0),gl_TexCoord[0].xy);
	if (dist <= 1){
	    maxAlpha = dist;

	    maxAlpha -= hardness;
	    if(hardness < 0)
	        hardness = 0;

    	maxAlpha = 1-maxAlpha;
	}

    maxAlpha *= maxAlpha;
    maxAlpha *= maxAlpha;

    maxAlpha *= alpha;

	gl_FragColor = vec4(col*maxAlpha, maxAlpha);
	//gl_FragColor = vec4(gl_TexCoord[0].xy, 0, 1);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}
