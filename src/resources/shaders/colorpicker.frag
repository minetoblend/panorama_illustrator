uniform float hueOffset;
uniform sampler2D sampler01;
uniform sampler2D sampler02;


vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main(){

    float u = gl_TexCoord[0].s;
    float v = 1-gl_TexCoord[0].t;
    float pi = 3.14159265;
    float distance = length(vec2(gl_TexCoord[0].s,1-gl_TexCoord[0].t));
    float angle = atan(v,u);
    vec4 color = vec4(0);

    float hueAlpha = texture2D(sampler02,gl_TexCoord[0].st).a;

    if(distance < 0.9){
        float val = (distance - 0.2) / 0.7;
        float sat = angle / pi * 2;
        color = vec4(hsv2rgb(vec3(hueOffset,sat,val)),1);
    }
    if(distance>0.9 && distance < 1){
        float t = angle/ pi * 2 - 0.5;

        t*= 2;
        t = t*t*t;
        t /= 2;

        float hue = t + hueOffset;
        if(t > -0.01 && t < 0.01){
            hue = hueOffset;
        }
        if(hue < 0){
            hue++;
        }
        hue = mod(hue,1);
        if(hueAlpha > 0.5)
            color = vec4(hsv2rgb(vec3(hue,1,1)),hueAlpha);
        else
            color = vec4(0.8);
    }

    color.a *= texture2D(sampler01,gl_TexCoord[0].st).a;

    gl_FragColor = color;
}