varying vec4 vertColor;
in vec4 gl_FragCoord;
uniform vec2 offset;


void main(){
	float b = 1.0;
	int x = floor(floor(gl_FragCoord.x - offset.x) / 20.0) + 1000;
	int y = floor(floor(gl_FragCoord.y + offset.y) / 20.0) + 1000;
    int total = x + y;

	if(mod(total + 1000,2) == 0){
	    b = 0.7;
    }

    gl_FragColor = vec4(b,b,b,vertColor.a);
}