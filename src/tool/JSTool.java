package tool;

import gui.GuiProperty;
import gui.InputDragEvent;
import gui.ToolProperties;
import gui.Window;
import kernel.ColorManager;
import kernel.FBO;
import kernel.Program;
import layer.BitmapLayer;
import org.lwjgl.opengl.*;
import resources.tools.brushes.BrushLocator;

import javax.script.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;

import static org.lwjgl.opengl.GL11.*;

public class JSTool extends Tool {

    ScriptEngine engine;

    static ScriptEngineManager manager = new ScriptEngineManager();

    /**
     * will create a tool based using the javascript file [id].js located in resources/tools/brushes/
     * An id of "simplebrush" will load /resources/tools/brushes/simplebrush.js
     *
     * @param id
     */
    public JSTool(String id) {
        engine = manager.getEngineByExtension("js");
        try {

            engine.put("propertyValues", new LinkedHashMap<String, GuiProperty>());
            //loads the base.js file first to provide extra functionality to be used in the brush file
            String js = readResourceAsString("base.js");
            engine.eval(js);
            js = readResourceAsString(id + ".js");
            engine.eval(js);
            ((Invocable) engine).invokeFunction("initProperties");
            getProperties();
        } catch (ScriptException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    /**
     * calls the onPress function in the javascript file
     */
    @Override
    public void onPress() {
        putEngineParams();
        try {
            ((Invocable) engine).invokeFunction("init");
            ((Invocable) engine).invokeFunction("onPress");
        } catch (ScriptException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    /**
     * calls the onDrag function in the javascript file
     */
    @Override
    public void onDrag(InputDragEvent event) {
        putEngineParams();
        try {
            ((Invocable) engine).invokeFunction("init");
            ((Invocable) engine).invokeFunction("onDrag", event);
            ((Invocable) engine).invokeFunction("cleanup");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDragPreview(InputDragEvent event, FBO fbo) {
        putEngineParamsPreview(fbo);
        try {
            ((Invocable) engine).invokeFunction("init");
            ((Invocable) engine).invokeFunction("onDrag", event);
            ((Invocable) engine).invokeFunction("cleanup");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    private void putEngineParamsPreview(FBO fbo) {
        engine.put("width", 200);
        engine.put("height", 80);
        engine.put("layer", null);
        engine.put("fbo", fbo);
        engine.put("activeColor", ColorManager.getActiveColor());
        engine.put("projected", false);
    }

    private void putEngineParams() {
        engine.put("rotX", Program.getProject().getRotX());
        engine.put("rotZ", Program.getProject().getRotZ());
        engine.put("zoom", Program.getProject().getZoom());
        engine.put("width", ((BitmapLayer) Program.getProject().getActiveLayer()).getWidth());
        engine.put("height", ((BitmapLayer) Program.getProject().getActiveLayer()).getHeight());
        engine.put("aspectRatio", Window.getCanvas().getAspectRatio());
        engine.put("layer", Program.getProject().getActiveLayer());
        engine.put("fbo", Program.getProject().getActiveLayer().getFbo());
        engine.put("activeColor", ColorManager.getActiveColor());
        engine.put("projected", Program.getProject().getDisplayProject());
    }

    public static String readResourceAsString(String name) {
        Scanner scanner = new Scanner(BrushLocator.class.getResourceAsStream(name), "UTF-8");
        String text = scanner.useDelimiter("\\A").next();

        scanner.close();
        return text;
    }

    @Override
    public LinkedHashMap<String, GuiProperty> getProperties() {
        return (LinkedHashMap<String, GuiProperty>) engine.get("propertyValues");
    }
}

