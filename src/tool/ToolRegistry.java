package tool;

import kernel.ColorManager;
import org.lwjgl.util.vector.Vector3f;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

/**
 * Registry containing all Tools(brushes, erasers, etc.) that can be used by the user
 */
public class ToolRegistry {

    static ArrayList<PropertyChangeListener> listeners = new ArrayList<>();

    public static Tool currentTool;
    private static ArrayList<Tool> tools = new ArrayList<>();


    public static void init() {
        registerTool(new JSTool("simplebrush"));
        //registerTool(new JSTool("pixelbrush"));

    }

    public static void registerTool(Tool tool) {
        tools.add(tool);

        setCurrentTool(tool);
    }

    public static ArrayList<Tool> getTools() {
        return tools;
    }

    public static Tool getCurrentTool() {
        return currentTool;
    }

    public static void setCurrentTool(Tool newTool) {
        notifyObservers(currentTool, newTool);
        ToolRegistry.currentTool = newTool;

    }

    private static void notifyObservers(Tool oldTool, Tool newTool) {
        for (PropertyChangeListener listener : listeners) {
            listener.propertyChange(new PropertyChangeEvent(ColorManager.class, "tool", oldTool, newTool));
        }
    }

    public static void addChangeListener(PropertyChangeListener listener) {
        listeners.add(listener);
    }
}
