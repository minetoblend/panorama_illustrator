package tool;

import gui.GuiProperty;
import gui.InputDragEvent;
import kernel.FBO;

import java.util.HashMap;

/**
 * superclass for all tools
 */
public abstract class Tool {
    public void onPress() {

    }

    public void onDrag(InputDragEvent event) {
    }


    public void onRelease() {
    }

    public abstract void onDragPreview(InputDragEvent event, FBO fbo);

    public abstract HashMap<String, GuiProperty> getProperties();
}
