package util;

import org.lwjgl.util.vector.Vector3f;

import java.awt.*;

public class ColorUtils {


    public static Vector3f ColortoVector3f(Color c) {
        return new Vector3f(c.getRed() / 255f, c.getGreen() / 255f, c.getBlue() / 255f);
    }
}
