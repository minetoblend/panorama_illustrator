package util;

import org.lwjgl.BufferUtils;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_BGRA;

public class GLUtils {

    public static int centeredQuadList, quadList, quadListInverted;


    public static void drawRectangle() {


        if (centeredQuadList == 0) {
            centeredQuadList = glGenLists(1);
            glNewList(centeredQuadList, GL_COMPILE);
            {
                glBegin(GL_QUADS);
                glTexCoord2f(-1, -1);
                glVertex2f(-1, -1);

                glTexCoord2f(1, -1);
                glVertex2f(1, -1);

                glTexCoord2f(1, 1);
                glVertex2f(1, 1);

                glTexCoord2f(-1, 1);
                glVertex2f(-1, 1);
                glEnd();
            }
            glEndList();
        }
        glCallList(centeredQuadList);
    }

    public static void drawRectangle(float x, float y, float w, float h) {
        glPushMatrix();
        glTranslatef(x, y, 0);
        glScalef(w, h, 1);

        drawRectangle();

        glPopMatrix();
    }

    public static void drawRectangle(float x, float y, float w, float h,float angle) {
        glPushMatrix();
        glTranslatef(x, y, 0);
        glRotatef(angle, 0, 0, 1 );
        glScalef(w, h, 1);

        drawRectangle();

        glPopMatrix();
    }

    public static int getTextureFromBufferedImage(BufferedImage img) {
        int glTexture = glGenTextures();

        ByteBuffer buf = null;
        buf = getBufferFromImage(img);

        glBindTexture(GL_TEXTURE_2D, glTexture);
        {
            //Setup filtering, i.e. how OpenGL will interpolate the pixels when scaling up or down
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            //Setup wrap mode, i.e. how OpenGL will handle pixels outside of the expected range
            //Note: GL_CLAMP_TO_EDGE is part of GL12
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.getWidth(), img.getHeight(), 0, GL_BGRA, GL_UNSIGNED_BYTE, buf);
        }
        glBindTexture(GL_TEXTURE_2D, 0);
        return glTexture;
    }

    private static ByteBuffer getBufferFromImage(BufferedImage image) {
        ByteBuffer textureData = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * Integer.SIZE);

        int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

        for (int i : pixels) {
            textureData.putInt(i);
        }
        textureData.flip();
        return textureData;
    }

    public static void fillRectangleC(float x, float y, float width, float height, float angle) {
        if (quadList == 0) {
            quadList = glGenLists(1);
            glNewList(quadList, GL_COMPILE);
            {
                glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2f(0, 0);

                glTexCoord2f(1, 0);
                glVertex2f(1, 0);

                glTexCoord2f(1, 1);
                glVertex2f(1, 1);

                glTexCoord2f(0, 1);
                glVertex2f(0, 1);
                glEnd();
            }
            glEndList();
        }
        glPushMatrix();
        glTranslatef(x, y, 0);
        glScalef(width, height, 1);
        glRotatef(angle, 0, 0, 1);
        glCallList(quadList);
        glPopMatrix();
    }

    static int quadListOutline;

    public static void drawRectangleC(float x, float y, float width, float height, float angle) {
        if (quadListOutline == 0) {
            quadListOutline = glGenLists(1);
            glNewList(quadListOutline, GL_COMPILE);
            {
                glBegin(GL_LINE_STRIP);

                glVertex2f(0, 0);


                glVertex2f(1, 0);


                glVertex2f(1, 1);


                glVertex2f(0, 1);

                glVertex2f(0, 0);
                glEnd();
            }
            glEndList();
        }
        glPushMatrix();
        glTranslatef(x, y, 0);
        glScalef(width, height, 1);
        glRotatef(angle, 0, 0, 1);
        glCallList(quadListOutline);
        glPopMatrix();
    }

    public static void drawRectangleCInverted(float x, float y, int width, int height, float angle) {
        if (quadListInverted == 0) {
            quadListInverted = glGenLists(1);
            glNewList(quadListInverted, GL_COMPILE);
            {
                glBegin(GL_QUADS);
                glTexCoord2f(0, 1);
                glVertex2f(0, 0);

                glTexCoord2f(1, 1);
                glVertex2f(1, 0);

                glTexCoord2f(1, 0);
                glVertex2f(1, 1);

                glTexCoord2f(0, 0);
                glVertex2f(0, 1);
                glEnd();
            }
            glEndList();
        }
        glPushMatrix();
        glTranslatef(x, y, 0);
        glScalef(width, height, 1);
        glRotatef(angle, 0, 0, 1);
        glCallList(quadListInverted);
        glPopMatrix();
    }

    public static void setupDefaultTextureParams() {
        //Setup filtering, i.e. how OpenGL will interpolate the pixels when scaling up or down
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //Setup wrap mode, i.e. how OpenGL will handle pixels outside of the expected range
        //Note: GL_CLAMP_TO_EDGE is part of GL12
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    static int quarterCircleList;

    public static void drawRoundedRectangle(float x, float y, float width, float height, float radius) {

        if(width < 0)
            width = 0;
        if(height < 0)
            height = 0;
        if (radius > width / 2)
            radius = width / 2;
        if (radius > height / 2)
            radius = height / 2;

        fillRectangleC(x, y + radius, width, height - radius * 2, 0);

        fillRectangleC(x + radius, y, width - radius * 2, radius, 0);

        fillRectangleC(x + radius, y + height - radius, width - radius * 2, radius, 0);

        drawQuarterCircle(x + width - radius, y + height - radius, radius, radius, 0);
        drawQuarterCircle(x + radius, y + height - radius, radius, radius, 90);
        drawQuarterCircle(x + radius, y + radius, radius, radius, 180);
        drawQuarterCircle(x + width - radius, y + radius, radius, radius, 270);
    }

    private static void drawQuarterCircle(float x, float y, float w, float h, float rot) {
        if (quarterCircleList == 0) {
            generateQuarterCircle();
        }
        glPushMatrix();

        glTranslatef(x, y, 0);
        glScalef(w, h, 1);
        glRotatef(rot, 0, 0, 1);
        glCallList(quarterCircleList);
        glPopMatrix();
    }

    private static void generateQuarterCircle() {
        quarterCircleList = glGenLists(1);

        glNewList(quarterCircleList, GL_COMPILE);

        glBegin(GL_TRIANGLE_FAN);

        glVertex2f(0, 0);
        for (int i = 0; i <= 16; i++) {
            float f = i / 16f;
            float x = (float) Math.cos(Math.toRadians(f * 90));
            float y = (float) Math.sin(Math.toRadians(f * 90));
            glVertex2f(x, y);
        }

        glEnd();

        glEndList();
    }


}
