package kernel;

import org.lwjgl.opengl.EXTFramebufferObject;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.EXTFramebufferObject.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;


/**
 * Class that allows quickly creating FBOs
 */
public class FBO {

    private final int glTexture;
    private final int frameBuffer;

    public FBO(int fbo, int texture) {
        this.glTexture = texture;
        this.frameBuffer = fbo;

    }

    public FBO(int glTexture) {
        this.glTexture = glTexture;
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);


        frameBuffer = glGenFramebuffersEXT();


        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBuffer);

        glBindTexture(GL_TEXTURE_2D, glTexture);

        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, glTexture, 0);

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);


    }

    public void bind() {

        glUseProgram(0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBuffer);
    }

    public void unbind() {
        glUseProgram(0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    }

    public int getFrameBuffer() {
        return frameBuffer;
    }

    public int getGlTexture() {
        return glTexture;
    }
}
