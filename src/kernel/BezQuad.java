package kernel;

import org.lwjgl.util.vector.Vector2f;

public class BezQuad extends BezierCurve{
    final Vector2f control;
    final Vector2f control2;

    public BezQuad(Vector2f p1, Vector2f control, Vector2f control2, Vector2f p2) {
        this.p1 = p1;
        this.control = control;
        this.control2 = control2;
        this.p2 = p2;
    }

    public float evaluateF(float f) {
        float a1 = mix(p1.y, control.y, f);
        float b1 = mix(control.y, control2.y, f);
        float c1 = mix(control2.y, p2.y, f);

        float a2 = mix(a1, b1, f);
        float b2 = mix(b1, c1, f);
        
        return mix(a2, b2, f);
    }


    private float mix(float a, float b, float f) {
        return a + (b - a) * f;
    }



    public Vector2f getControl() {
        return control;
    }

    public Vector2f getControl2() {
        return control2;
    }
}
