package kernel;

public interface Function {
    public float evaluateF(float pos);
}
