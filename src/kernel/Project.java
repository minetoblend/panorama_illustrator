package kernel;


import gui.InputDragEvent;
import gui.NewProjectDialog;
import gui.Window;
import layer.BackgroundLayer;
import layer.BitmapLayer;
import layer.ProjectLayer;
import tool.ToolRegistry;
import util.GLUtils;
import util.ShaderUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import static org.lwjgl.opengl.EXTFramebufferObject.*;
import static org.lwjgl.opengl.EXTFramebufferObject.GL_COLOR_ATTACHMENT0_EXT;
import static org.lwjgl.opengl.EXTFramebufferObject.glFramebufferTexture2DEXT;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL12.GL_BGRA;
import static org.lwjgl.opengl.GL20.*;

public class Project {

    /**
     * Pointer to the projection shader and all values passed to the shader.
     * see /resources/shaders/equirectangular.frag
     */
    private static int projectionShader, uniformRotX, uniformRotZ, uniformZoom, uniformAspectRatio;
    /**
     * up/down of the camera
     */
    private float rotX = 0;
    /**
     * left/right rotation of the camera
     */
    private float rotZ;
    /**
     * zoom of the camera -> field of view
     */
    private float zoom = 1f;

    /**
     * shader to unpremultiply the premultiplied alpha blending for correct blending of transparent colors
     */
    private static int unpremulShader;
    private ProjectLayer activeLayer;

    private String name;
    /**
     * File path where the project is saved.
     */
    private File file;

    private BackgroundLayer backgroundLayer;
    private ArrayList<ProjectLayer> layers;
    private int width, height;
    private boolean displayProjected = true;

    public Project(int width, int height) {
        this.name = "untitled";
        this.width = width;
        this.height = height;

        layers = new ArrayList<ProjectLayer>();
        backgroundLayer = new BackgroundLayer(Color.white);

        addLayer(new BitmapLayer(this, width, height));
    }

    public static int getProjectionShader() {
        if (projectionShader == 0) {
            projectionShader = ShaderUtils.getShader("equirectangular");
        }

        uniformRotX = glGetUniformLocation(projectionShader, "rotX");
        uniformRotZ = glGetUniformLocation(projectionShader, "rotZ");
        uniformZoom = glGetUniformLocation(projectionShader, "zoom");
        uniformAspectRatio = glGetUniformLocation(projectionShader, "aspectRatio");
        return projectionShader;
    }

    public static void createNew() {
        NewProjectDialog.showDialog();

    }

    private void addLayer(ProjectLayer layer) {
        this.layers.add(layer);
        this.activeLayer = layer;
    }

    public void render() {
        handleInputEvents();

        if(Window.getCanvas().getResized()){
            Window.getCanvas().setResized(false);
            generateFBO(Window.getCanvas().getWidth(),Window.getCanvas().getHeight());
        }

        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);

        glViewport(0,0,Window.getCanvas().getWidth(), Window.getCanvas().getHeight());

        glUseProgram(getProjectionShader());
        glUniform1f(uniformAspectRatio, Window.getCanvas().getAspectRatio());
        glUniform1f(uniformRotX, rotX);
        glUniform1f(uniformRotZ, rotZ);
        glUniform1f(uniformZoom, 1/zoom);
        glUseProgram(0);




        backgroundLayer.render();
        renderList(this.layers);

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

        glBindTexture(GL_TEXTURE_2D, fboTexture);

        if(unpremulShader == 0){
            unpremulShader = ShaderUtils.getShader("un-premultiply");
        }

        glUseProgram(unpremulShader);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0,1,0,1,-1,1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glColor3f(1,1,1);
        GLUtils.fillRectangleC(0,0,1,1,0);
        glUseProgram(0);


    }

    private void handleInputEvents() {
        for(int i = 0; i < 5 && events.size()>0; i++){
            /*
            TODO: figure out why events.getFirst() occasionaly fails even though there are elements in the LinkedList
             */
            InputDragEvent event = events.get(0);
            events.remove(0);
            if(event instanceof  InputDragEvent){
                ToolRegistry.getCurrentTool().onDrag(event);
            }
        }
    }

    private void renderList(ArrayList<ProjectLayer> layers) {
        for (int i = layers.size() - 1; i >= 0; i--) {
            ProjectLayer layer = layers.get(i);
            layer.render();
        }
    }

    /**
     * Creates the name for a new layer when it is being created
     * @return
     */
    public String getNextLayerName() {
        String curName = "Layer 1";
        int count = 2;

        while (getLayerByName(curName) != null) {
            curName = String.format("Layer ",count++);
        }

        return curName;
    }

    private ProjectLayer getLayerByName(String name) {
        for (ProjectLayer layer : layers) {
            if (layer.getName().equals(name)) {
                return layer;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public float getRotX() {
        return rotX;
    }

    public void setRotX(float rotX) {
        this.rotX = rotX;
    }

    public float getRotZ() {
        return rotZ;
    }

    public void setRotZ(float rotZ) {
        this.rotZ = rotZ;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    public void addInputEvent(InputDragEvent event) {
        events.add(event);

    }

    ArrayList<InputDragEvent> events = new ArrayList<>();

    public ProjectLayer getActiveLayer() {
        return activeLayer;
    }

    public DisplayMode getDisplayMode() {
        return displayMode;
    }

    DisplayMode displayMode = DisplayMode.PROJECTED;

    public boolean getDisplayProject() {
        return displayProjected;
    }

    public enum DisplayMode{
        PROJECTED, FLAT
    }

    int fboTexture, fbo;

    private void generateFBO(int width, int height) {
        if (fbo != 0) {
            glDeleteFramebuffersEXT(fbo);
        }
        if (fboTexture != 0) {
            glDeleteTextures(fboTexture);
        }

        fboTexture = glGenTextures();
        fbo = glGenFramebuffersEXT();

        glBindTexture(GL_TEXTURE_2D, fboTexture);
        {
            //Setup filtering, i.e. how OpenGL will interpolate the pixels when scaling up or down
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            //Setup wrap mode, i.e. how OpenGL will handle pixels outside of the expected range
            //Note: GL_CLAMP_TO_EDGE is part of GL12
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, (ByteBuffer) null);
        }
        glBindTexture(GL_TEXTURE_2D, 0);


        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
        glBindTexture(GL_TEXTURE_2D, fboTexture);

        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, fboTexture, 0);

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public int getFbo() {
        return fbo;
    }

    public int getFboTexture() {
        return fboTexture;
    }
}