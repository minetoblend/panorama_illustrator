package kernel;

import gui.Renderer;
import gui.Window;
import javafx.stage.FileChooser;
import tool.ToolRegistry;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Window.init();

        Renderer.run();

        startConsoleInputListener();
    }

    /**
     * listens for javascript input in the console
     */
    private static void startConsoleInputListener() {
        ScriptEngine engine = new ScriptEngineManager().getEngineByExtension("js");
        Scanner scanner = new Scanner(System.in);
        while(true){
            String line = scanner.nextLine();
            try {
                engine.eval(line);
            } catch (ScriptException e) {
                e.printStackTrace();
            }
        }
    }


    static Project project;

    public static Project getProject() {
        return project;
    }

    public static void setProject(Project p) {
        project = p;
        Window.getFrame().setTitle(p.getName());
    }


}
