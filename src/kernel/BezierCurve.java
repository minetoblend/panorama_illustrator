package kernel;

import org.lwjgl.util.vector.Vector2f;

public abstract class BezierCurve {
    Vector2f p1;
    Vector2f p2;

    public Vector2f getP1() {
        return p1;
    }

    public Vector2f getP2() {
        return p2;
    }

    public abstract float evaluateF(float pos);
}
