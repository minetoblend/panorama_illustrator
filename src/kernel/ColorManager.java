package kernel;

import gui.ToolProperties;
import org.lwjgl.util.vector.Vector3f;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Observable;

/**
 * Manages all colors in the software(e.g. currently selected color, secondary color)
 */
public class ColorManager {

    static ArrayList<PropertyChangeListener> listeners = new ArrayList<>();

    static Vector3f primaryColor = new Vector3f();
    static Vector3f secondaryColor = new Vector3f(1, 1, 1);
    static int activeColor = 0;


    public static Vector3f getPrimaryColor() {
        return primaryColor;
    }

    public static Vector3f getActiveColor() {
        if (activeColor == 0)
            return primaryColor;
        return secondaryColor;
    }

    public static void setActiveColor(Vector3f c) {
        if (activeColor == 0) {
            notifyObservers(primaryColor, c);
            primaryColor = c;

        } else {
            notifyObservers(secondaryColor, c);
            secondaryColor = c;
        }
    }

    private static void notifyObservers(Vector3f oldC, Vector3f newC) {
        for (PropertyChangeListener listener : listeners) {
            listener.propertyChange(new PropertyChangeEvent(ColorManager.class, "color", oldC, newC));
        }
    }

    public static void addChangeListener(PropertyChangeListener listener) {
        listeners.add(listener);
    }

    /*
    TODO: add color palette
     */
}
