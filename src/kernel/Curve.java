package kernel;

import kernel.BezQuad;
import kernel.BezTriple;
import kernel.BezierCurve;
import org.lwjgl.util.vector.Vector2f;

import java.util.*;

public class Curve {

    ArrayList<Vector2f> points = new ArrayList<>();

    ArrayList<BezierCurve> curves = new ArrayList<>();

    public Curve(String value) {
        switch (value) {
            case "default":
            default:
                addPoint(new Vector2f(0, 0));
                addPoint(new Vector2f(1, 1));

                break;
        }
    }

    public void movePoint(Vector2f p, float x, float y) {
        p.x = x;
        p.y = y;

        if (p.x < 0)
            if (points.indexOf(p) != 0)
                points.remove(p);
            else
                p.x = 0;

        if (p.x > 1)
            if (points.indexOf(p) != points.size() - 1)
                points.remove(p);
            else
                p.x = 0;

        if (p.y < 0)
            p.y = 0;
        if (p.y > 1)
            p.y = 1;


        Collections.sort(points, new Comparator<Vector2f>() {
            @Override
            public int compare(Vector2f o1, Vector2f o2) {
                return Float.compare(o1.x, o2.x);
            }
        });
        buildCurves();
    }

    public void addPoint(Vector2f point) {
        points.add(point);
        Collections.sort(points, new Comparator<Vector2f>() {
            @Override
            public int compare(Vector2f o1, Vector2f o2) {
                return Float.compare(o1.x, o2.x);
            }
        });
        buildCurves();
    }

    private void buildCurves() {
        curves.clear();

        if (points.size() == 0) {
            curves.add(new BezTriple(
                    new Vector2f(0, 0),
                    new Vector2f(.5f, 0),
                    new Vector2f(.5f, 0)
            ));
        } else if (points.size() == 1) {
            float val = points.get(0).y;
            curves.add(new BezTriple(
                    new Vector2f(0, val),
                    new Vector2f(.5f, val),
                    new Vector2f(.5f, val)
            ));
        } else if (points.size() == 2) {
            curves.add(new BezTriple(points.get(0), mixVectors(points.get(0), points.get(1), 0.5f), points.get(1)));
        } else if (points.size() > 2) {
            {
                Vector2f p1 = points.get(0);
                Vector2f p2 = points.get(1);
                Vector2f p3 = points.get(2);

                float slope12 = (p1.y - p2.y) / (p1.x - p2.x);
                float slope23 = (p2.y - p3.y) / (p2.x - p3.x);



                float slope = mix(slope12, slope23, 0.5f);
                float x = mix(p1.x, p2.x, .5f);
                float y = p2.y - slope * 0.5f * (p2.x - p1.x);

                curves.add(new BezTriple(
                        p1, new Vector2f(x, y), p2
                ));
            }


            for (int i = 1; i < points.size() - 2; i++) {
                Vector2f p1 = points.get(i - 1);
                Vector2f p2 = points.get(i);
                Vector2f p3 = points.get(i + 1);
                Vector2f p4 = points.get(i + 2);

                float slope12 = (p1.y - p2.y) / (p1.x - p2.x);
                float slope23 = (p2.y - p3.y) / (p2.x - p3.x);
                float slope34 = (p3.y - p4.y) / (p3.x - p4.x);

                float slopeP2 = mix(slope12, slope23, .5f);
                float slopeP3 = mix(slope23, slope34, .5f);

                float x = mix(p2.x, p3.x, 0.333f);
                float x2 = mix(p2.x, p3.x, 0.666f);

                float y = p2.y + slopeP2 * 0.333f * (p3.x - p2.x);
                float y2 = p3.y - slopeP3 * 0.333f * (p3.x - p2.x);

                BezQuad curve = new BezQuad(p2, new Vector2f(x, y), new Vector2f(x2, y2), p3);
                curves.add(curve);
            }

            {
                Vector2f p1 = points.get(points.size() - 3);
                Vector2f p2 = points.get(points.size() - 2);
                Vector2f p3 = points.get(points.size() - 1);

                float slope12 = (p1.y - p2.y) / (p1.x - p2.x);
                float slope23 = (p2.y - p3.y) / (p2.x - p3.x);
                float slope = mix(slope12, slope23, 0.5f);

                float x = mix(p2.x, p3.x, 0.333f);
                float y = p2.y + slope * 0.5f * (p3.x - p2.x);

                curves.add(new BezTriple(
                        p2, new Vector2f(x, y), p3
                ));
            }
        }
    }


    public float evaluate(float pos) {
        BezierCurve curve = null;

        for (BezierCurve c : curves) {


            if (c.getP2().x >= pos) {
                curve = c;
                break;
            }
        }

        float f = (pos - curve.getP1().x) / (curve.getP2().x - curve.getP1().x);
        float val = curve.evaluateF(f);
        if (val < 0)
            val = 0;
        if (val > 1)
            val = 1;
        return val;
    }

    float mix(float a, float b, float f) {
        return a + (b - a) * f;
    }

    private Vector2f mixVectors(Vector2f a, Vector2f b, float f) {
        return new Vector2f(mix(a.x, b.x, f), mix(a.y, b.y, f));
    }

    public ArrayList<Vector2f> getPoints() {
        return points;
    }
}