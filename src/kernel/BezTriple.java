package kernel;

import org.lwjgl.util.vector.Vector2f;

public class BezTriple extends BezierCurve {

    final Vector2f control;

    public BezTriple(Vector2f p1, Vector2f control, Vector2f p2) {
        this.p1 = p1;
        this.control = control;
        this.p2 = p2;
    }

    public float evaluateF(float f) {
        return mix(mix(p1.y, control.y, f), mix(control.y, p2.y, f), f);
    }


    private float mix(float a, float b, float f) {
        return a + (b - a) * f;
    }


    public Vector2f getControl() {
        return control;
    }



    @Override
    public String toString() {
        return "BezTriple{" +
                "p1=" + p1 +
                ", control=" + control +
                ", p2=" + p2 +
                '}';
    }
}
