package blending;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.lwjgl.opengl.GL11.*;

public class BlendingMode {

    /**
     * The name of this Blending Mode
     */
    String name;

    /**
     * In RGBA mode, pixels can be drawn using a function that blends the incoming (source) RGBA
     * values with the RGBA values that are already in the frame buffer (the destination values).
     * <p>
     * The srcFac and destFac are fed into the glBlendFunc function to specify how the combined
     * color will be calculated.
     * <p>
     * Default Blending:
     * srcFac: GL_SRC_ALPHA
     * destFac: GL_ONE_MINS_SRC_ALPHA
     * <p>
     * See <a href="https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glBlendFunc.xml">https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glBlendFunc.xml</a>
     */
    int srcFac, destFac;

    private static HashMap<String, Integer> existingBlendingModes;

    /**
     * initializes the list of possible blending modes
     */
    static {
        existingBlendingModes = new HashMap<>();
        existingBlendingModes.put("GL_ZERO", GL_ZERO);
        existingBlendingModes.put("GL_ZERO", GL_ONE);

        existingBlendingModes.put("GL_SRC_COLOR", GL_SRC_COLOR);
        existingBlendingModes.put("GL_ONE_MINUS_SRC_COLOR", GL_ONE_MINUS_SRC_COLOR);
        existingBlendingModes.put("GL_DST_COLOR", GL_DST_COLOR);
        existingBlendingModes.put("GL_ONE_MINUS_DST_COLOR", GL_ONE_MINUS_DST_COLOR);

        existingBlendingModes.put("GL_SRC_ALPHA", GL_SRC_ALPHA);
        existingBlendingModes.put("GL_ONE_MINUS_SRC_ALPHA", GL_ONE_MINUS_SRC_ALPHA);
        existingBlendingModes.put("GL_DST_ALPHA", GL_DST_ALPHA);
        existingBlendingModes.put("GL_ONE_MINUS_DST_ALPHA", GL_ONE_MINUS_DST_ALPHA);

        existingBlendingModes.put("GL_CONSTANT_COLOR", GL_CONSTANT_COLOR);
        existingBlendingModes.put("GL_ONE_MINUS_CONSTANT_COLOR", GL_ONE_MINUS_CONSTANT_COLOR);
        existingBlendingModes.put("GL_CONSTANT_ALPHA", GL_CONSTANT_ALPHA);
        existingBlendingModes.put("GL_ONE_MINUS_CONSTANT_ALPHA", GL_ONE_MINUS_CONSTANT_ALPHA);

        existingBlendingModes.put("GL_SRC_ALPHA_SATURATE", GL_SRC_ALPHA_SATURATE);
    }

    /**
     * Creates a Blending Mode with the given name and
     *
     * @param name
     * @param srcFac
     * @param destFac
     */
    public BlendingMode(String name, int srcFac, int destFac) {

        if ( !existingBlendingModes.containsValue(srcFac) ) {
            throw new NotABlendingFactorException(srcFac);
        }
        if ( !existingBlendingModes.containsValue(destFac) ) {
            throw new NotABlendingFactorException(destFac);
        }

        this.name = name.trim();
        this.srcFac = srcFac;
        this.destFac = destFac;
    }

    public String getName() {
        return name;
    }

    public int getDestFac() {
        return destFac;
    }

    public int getSrcFac() {
        return srcFac;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public void setSrcFac(int srcFac) {
        this.srcFac = srcFac;
        if ( !existingBlendingModes.containsValue(srcFac) ) {
            throw new NotABlendingFactorException(srcFac);
        }
    }

    public void setDestFac(int destFac) {
        this.destFac = destFac;
        if ( !existingBlendingModes.containsValue(destFac) ) {
            throw new NotABlendingFactorException(destFac);
        }
    }

    public static Set<Map.Entry<String, Integer>> getExistingBlendingModes() {
        return existingBlendingModes.entrySet();
    }

    public static Set<String> getExistingBlendingModeNames() {
        return existingBlendingModes.keySet();
    }
}
