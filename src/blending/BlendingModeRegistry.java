package blending;
import static org.lwjgl.opengl.GL11.*;

import java.util.Collection;
import java.util.HashMap;

/**
 * contains a list of all possible blending modes to be choosable for a project layer
 * 
 */
public class BlendingModeRegistry {
    private final static HashMap<String, BlendingMode> blendingModes = new HashMap<>();

    /**
     * creates the default blending modes at program startup
     * they will be required during entire runtime so no real point to lazy loading
     */
    static{
        registerBlendingMode(new BlendingMode("normal",GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
    }

    static void registerBlendingMode(BlendingMode b){

        if(b == null)
            throw new NullPointerException();

        blendingModes.put(b.name.toLowerCase(),b);
    }

    public static BlendingMode get(String name){
        return blendingModes.get(name);
    }

    public static HashMap<String, BlendingMode> getBlendingModes() {
        return blendingModes;
    }

    public static Collection<BlendingMode> getBlendingModesAsCollection() {
        return blendingModes.values();
    }
}
