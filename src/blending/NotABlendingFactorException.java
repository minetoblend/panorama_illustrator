package blending;

import java.util.Arrays;

public class NotABlendingFactorException extends RuntimeException{

    public NotABlendingFactorException(int value){
        super(
                value + " is not an existing blending mode. Acceptable values: \n" +
                        BlendingMode.getExistingBlendingModeNames().toString() + "\n"+
                        "All acceptable values can be found as static fields in org.lwjgl.opengl.GL11"
        );
    }
}
