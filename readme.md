# Panorama Drawing Software

An illustration software for creating equirectangular drawings.

## Prerequisites
Due to difficulties with the Nashorn ScriptEngien in Java 8 and above, the project can currently only be run in Java 7.<br/>
Pressure sensitivity is only supported if a graphics tablet is connected to the machine.<br/>
If no graphic tablet is connected / the drawing is performed by the mouse, a pressure of 1.0 at all times will be used instead.

## Installing and Building/Running

Clone git repo
```
git clone https://gitlab.com/minetoblend/panorama_drawing_software.git
```

Configure project in IDE
```
In Eclipse:
Build Path Settings ->  add Jars (everything in lib folder)
                        for jwlgj.jar:
                            set /native/lwjgl/<os>/ as native library location
                        for jpen.jar
                            set /native/jpen/ as native library location
Run with main class kernel.Program
In IntelliJ:
Library Settings(F4) -> add the following folders to the libraries:
                            /lib/
                            /native/lwjgl/<your os>
                            /native/jpen/
Create Run configuration with main class kernel.Program
```

## How to use
Draw with left mouse click or stylus<br/>
Rotate view with middle mouse click/touch input<br/>
Zoom in and out using mouse wheel